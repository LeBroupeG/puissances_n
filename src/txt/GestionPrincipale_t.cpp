#include "GestionPrincipale_t.h"
using namespace std;

GestionPrincipale_t::GestionPrincipale_t()
{
}

GestionPrincipale_t::~GestionPrincipale_t()
{
        if (partiePrincipale != nullptr)
    {
        delete partiePrincipale;
        partiePrincipale = nullptr;
    }
}

void GestionPrincipale_t::afficherEcranAccueil()
{
    cout << "Bienvenu dans le Puissance N (ver 1.0 proposé par Romain Genty / Noé Dubois / Tim Kosak" << endl
         << endl;
}

void GestionPrincipale_t::afficherEcranFin(){
    ecrireScore();
    if(score2 != score1){
    cout << "La victoire revient au joueur n°";
    if(score2 > score1) cout << 2;
    else cout << 1;

    cout << " Congratulations !! " << endl;}
    else
        cout<<"Egalite !"<<endl;
    
}

void GestionPrincipale_t::init()
{

    cout << " -> Combien de partie(s) désirez-vous effectuer ? : ";
    cin >> nbChainage;

    if(nbChainage > 1) estChainee = true;
    else estChainee = false;

    cout << endl
         << " -> Mode aléatoire à chaque partie ? (1/0) ";
    cin >> modeDeJeuAleaChainage;

    cout << endl<< "Taille de grille aléatoire d'une partie à l'autre ? : (0/1) ";
    cin >> tailleGrilleAleaChainage;

    
    cout << endl
         << " -> Souhaitez jouer contre notre IA ? : (0/1)";
    cin >> iAActivee;



    cout << endl<< "Limite de temps (par défaut 20sec) ? : (0/1) ";
    cin >> tempsLimitAleaChainage;
}

void GestionPrincipale_t::creerSession(){
    unsigned int modeDeJeu, largeur, hauteur;
    float tempsLimite;

    if(modeDeJeuAleaChainage) modeDeJeu = rand() % 5;
    else{
        cout << "Mode de jeu ? Normal (0), shifting vertical (1), shifting horizontal (2), Insertion Alternée (3), Gravité changeante (4), Centre de gravité (5): ";
        cin >> modeDeJeu;
    }
    
    if(tailleGrilleAleaChainage){
        largeur = rand() % 7 + 7;
        hauteur = rand() % 7 + 7;
    }else{
        cout << "Largeur ? : ";
        cin >> largeur; 
        cout << "Hauteur ? : ";
        cin >> hauteur;
    }

    if(tempsLimitAleaChainage) tempsLimite = rand() % 20 + 5;
    else tempsLimite = 20;

    if(partiePrincipale != nullptr) delete partiePrincipale;
    //partiePrincipale = new Partie_t(gameMode, largeur, hauteur, true, timeLimit, false);
    partiePrincipale = new Partie_t(modeDeJeu, largeur, hauteur, false, tempsLimite, iAActivee);
}

void GestionPrincipale_t::ecrireScore(){
    cout << "SCORE : " << endl
    << "    Joueur 1 : " << score1 << endl
    << "    Joueur 2 : " << score2 << endl;
}

void GestionPrincipale_t::nettoyerEcran(){
    for(int x = 0; x < 100; x++) cout << endl;
}