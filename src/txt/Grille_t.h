#ifndef GRILLE_T_H
#define GRILLE_T_H

#include "../core/Grille_c.h"
#include "Pion_t.h"

/**
 * @brief grille destinée à l'affichage dans le terminal
 */
class Grille_t : public Grille_c
{
	protected:
	char charVide = ' ';
    public:
    
 /**
  * @brief Constructeur de la Grille permettant de faire une grille de la taille que l'on désire
  * @param largeur
  * @param hauteur
  */
	Grille_t(int largeur, int hauteur);
    /**
     * @brief destructeur
     * @param grille
     */
    Grille_t(const Grille_c& grille);

	    /**
    *   @brief Permet d'inserer un pion dans la grille
    *   @param posX : Position sur X
    *   @param posY : Position sur Y
    *   @return Vrai si l'inserton s'est bien passée, faux sinon
    **/
    bool ajouterPion(const int joueur, unsigned int colonne, bool milieu, bool insererParDessus);
 /**
  * @brief affiche une ligne de la grille dans le terminal
  * @return (void)
  */
	void dessinerLigne();
 /**
  * @brief affiche la grille dans le terminal
  * @return (void)
  */
	void dessinerGrille();
};

#endif