#include "Grille_t.h"
#include "Pion_t.h"
#include <iostream>
using namespace std;

Grille_t::Grille_t(int width, int heigh) : Grille_c(width, heigh)
{

}

Grille_t::Grille_t(const Grille_c& grille) : Grille_c(grille)
{
    
}

void Grille_t::dessinerLigne(){
    for(int y = 0; y < nbColonne; y++){
            cout << "[" << y << "] ";
        }
}

void Grille_t::dessinerGrille(){

    cout << "y" << endl << "^";
    dessinerLigne();
    cout << endl;

    for(int y = nbLigne - 1; y >= 0; y--){
        cout << "| ";
        for(int x = 0; x < nbColonne; x++){
            Pion_t* buff = (Pion_t*)getPion(x, y);
            if(buff != nullptr) cout << /*buff->t_texture*/ buff->idJoueur << " | ";
            else cout << " " << " | ";
        }
        cout << endl;
    }
    cout << " ";
    dessinerLigne();
    cout << "x" << endl; 
}

bool Grille_t::ajouterPion(const int joueur, unsigned int column, bool mid, bool UpDown)
{
    Pion_t *temp = new Pion_t((char) joueur);
    return insererPion(column, temp, mid, UpDown);
}