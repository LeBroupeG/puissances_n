#ifndef PION_T_H
#define PION_T_H

#include "../core/Pion_c.h"

/**
 * @brief Pion destiné à l'affichage dans un terminal
 */
class Pion_t : public Pion_c
{
protected:
public:
	char texture;

	/**
  * @brief Constructeur de la classe pion permettant de mettre un caractère poru différencier les pions
  * @param texture_
  */
	Pion_t(const char &texture_);
 /**
  * @brief destructeur de la classe
  */
	~Pion_t();
};

#endif