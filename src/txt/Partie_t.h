#ifndef PARTIE_TXT_H
#define PARTIE_TXT_H

#include "../core/Partie_c.h"
#include "Grille_t.h"

#include <iostream>
#include <assert.h>

using namespace std;

/**
 * @brief Partie destinée à l'affichage dans le terminal
 */
class Partie_t : public Partie_c
{
protected:
    /* data */

public:
    /**
     * @brief Costructeur par défaut
     */
    Partie_t();
    /**
     * @brief destructeur
     */
    ~Partie_t();
    /**
     * @brief constructeur permettant de créer une partie avec les paramètres demandés par l'utilisateur
     * @param modeJeu
     * @param int
     * @param int
     * @param estTempsLimite
     * @param tempsLimite
     * @param iAActive
     * @param niveauIA
     */
    Partie_t(int modeJeu, unsigned int largeurGrille, unsigned int hauteurGrille, bool estTempsLimite, float tempsLimite, bool iAActive, int niveauIA = 1);

    /**
     * @brief Initialise la partie
     * @return (void)
     */
    void initGame();
    /**
     * @brief Affiche la grille de la partie en cours dans le terminal
     * @return (void)
     */
    void dessinerGrille();
    /**
     * @brief nettoie le terminal
     * @return (void)
     */
    void nettoyerEcran();
    /**
     * @brief Permet de savoir si un joueur à gagner la partie ou non
     * @return 
     */
    int testerVictoire();
    /**
     * @brief Permet de faire jouer un joueur puis l'autre tour à tour
     * @return (void)
     */
    void faireJouerLesJoueurs();
    /**
     * @brief Affiche les paramètres de la partie en cours dans le terminal
     * @return (void)
     */
    void ecrireDetails();
};

#endif