#ifndef GESTION_PRINCIPALE_T
#define GESTION_PRINCIPALE_T

#include "Partie_t.h"
#include "../core/GestionPrincipale_c.h"

using namespace std;

/**
 * @brief Classe permettant de gérer tout le jeu en version textuelle (choix des paramètres, création et enchaînemnt d epartie, fin de jeu...)
 */
class GestionPrincipale_t : public GestionPrincipale_c
{
public:
    /**
     * @brief Constructeur de gestionnaire de jeu
     */
    GestionPrincipale_t();
    /**
     * @brief destructeur
     */
    ~GestionPrincipale_t();

    /**
     * @brief Affiche un menu dans le terminal permettant de sélectionner les paramètres des prochaines parties
     * @return (void)
     */
    void init();
    /**
     * @brief Créer une partie avec les paramètres spécifiés dans le menu
     * @return (void)
     */
    void creerSession();

    /**
     * @brief nettoie le terminal
     * @return (void)
     */
    void nettoyerEcran();
    /**
     * @brief AFfiche un message d'acceuil dans le terminal
     * @return (void)
     */
    void afficherEcranAccueil();
    /**
     * @brief Affiche le joueur gagnant dans le terminal
     * @return (void)
     */
    void afficherEcranFin();
    /**
     * @brief Affiche le score dans le terminal (pour un enchaînement de parties)
     * @return (void)
     */
    void ecrireScore();
};

#endif