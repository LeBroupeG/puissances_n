#include <iostream>

#include "GestionPrincipale_t.h"

using namespace std;

int main()
{
    GestionPrincipale_t mainManager;
    mainManager.prendreEnMain();

    return 0;
}