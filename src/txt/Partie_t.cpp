#include "Partie_t.h"

Partie_t::Partie_t(int gameMode_, unsigned int largeurGrille, unsigned int hauteurGrille, bool isTimeLimited, float timeLimit_, bool iAActivated_, int niveauIA)
    : Partie_c(gameMode_, largeurGrille, hauteurGrille, isTimeLimited, timeLimit_, iAActivated_, niveauIA)
{
    	if (!(largeurGrille <= 4 or hauteurGrille <= 4))
	{
    		switch (modeDeJeu)
		{
		case 0:
			grille = new Grille_t(largeurGrille, hauteurGrille);
			break;
		case 1:
			grille = new Grille_t(largeurGrille, hauteurGrille);
			break;
		case 2:
			grille = new Grille_t(largeurGrille, hauteurGrille);
			break;

		case 3:
			if ((hauteurGrille % 2) == 0)
			{
				grille = new Grille_t(largeurGrille, hauteurGrille + 1);
			}
			else
			{
				grille = new Grille_t(largeurGrille, hauteurGrille);
			}
			break;

		case 4:
			unsigned int h, t;
			h = hauteurGrille;
			t = largeurGrille;
			if (t < h)
			{
				if (h % 2 == 0)
				{
					t = h + 1;
					h++;
				}
				else
					t = h;
			}
			else
			{
				if (t % 2 == 0)
					t++;
				h = t;
			}
			grille = new Grille_t(t, h);
			break;
		case 5:
			grille = new Grille_t(largeurGrille, hauteurGrille);
			break;
		}
    }
    assert(grille != nullptr);
}

Partie_t::~Partie_t()
{
}

void Partie_t::faireJouerLesJoueurs()
{
    

    cout << "C'est au Joueur nb " << (tourJoueur1? 1 : 2) << " de jouer, colonne ? : ";
    int colonne = -1;
    int idPlayer;
    do
    {
        
        if (tourJoueur1)
        {
            if (iAActive)
            {
                colonne = intArt->jouer();
                cout << colonne;
            }
            else
                cin >> colonne;


            idPlayer = 1;
        }
        else
        {
            cin >> colonne;
            idPlayer = 2;

            if (iAActive)
                intArt->lAutreJoueurJoue(colonne);
        }
            
    } while (!((Grille_t *)grille)->ajouterPion(idPlayer, colonne, tomberAuMilieu(), insererPionsParDessus));
    tourJoueur1 = !tourJoueur1;
    leJoueurAJoue = true;
}

int Partie_t::testerVictoire()
{
    if (grille->unJoueurAGagne(4, 1))
    {
        if (iAActive)
            cout << "L'intelligence artificielle à gagné" << endl;
        else
            cout << "Le joueur 1 a gagné !" << endl;
        return 1;
    }
    else if (grille->unJoueurAGagne(4, 2))
    {
        cout << "Le joueur 2 a gagné !" << endl;
        return 2;
    }
    else
    {
        return 0;
    }
}

void Partie_t::dessinerGrille()
{
    ((Grille_t *)grille)->dessinerGrille();
}

void Partie_t::ecrireDetails()
{

    switch (modeDeJeu)
    {
    case 0:
        cout << "Mode Normal" << endl;
        break;
    case 1:
        cout << "Mode Shift Vertical" << endl;

        break;
    case 2:
        cout << "Shift Horizontal" << endl;
        break;
    case 3:
        cout << "Mode Insertion Alternée : " << tourAvantEvenement << endl
             << "Insertion par le ";
        if (insererPionsParDessus)
            cout << "haut";
        else
            cout << "bas";
        cout << endl;
        break;
    case 4:
        cout << "Mode Gravité Changeante : " << tourAvantEvenement << endl;
        break;
    case 5:
        cout << "Mode Centre de Gravité : " << tourAvantEvenement << endl;
        break;
    }
}

void Partie_t::nettoyerEcran()
{
    for (int x = 0; x < 100; x++)
        cout << endl;
}