#include "Grille_g.h"

Grille_g::Grille_g(RenderWindow *p_fenetre, int width, int heigh) : Grille_c(width, heigh)
{
    fenetre = p_fenetre;
    Vector2i size;

    size.x = p_fenetre->getSize().x;
    size.y = p_fenetre->getSize().y;

    ///calcul de la position et de la taille de la grille
    hauteurPX = size.y * 0.85;
    largeurPX = hauteurPX * nbColonne / nbLigne;
    if (largeurPX > size.x)
    {
        largeurPX = size.x * 0.85;
        hauteurPX = largeurPX * nbLigne / nbColonne;
    }
    pos.x = (size.x - largeurPX) / 2;
    pos.y = (size.y - hauteurPX) / 2;

    if (nbLigne < nbColonne)
    {
        rayon = largeurPX / (2 * (nbColonne + 1));
        dist = (largeurPX - 2 * nbColonne * rayon) / nbColonne;
    }
    else
    {
        rayon = hauteurPX / (2 * (nbLigne + 1));
        dist = (hauteurPX - 2 * nbLigne * rayon) / nbLigne;
    }
    //cout << "Rows/Column : " << nbLine << " / " << nbColumn << endl;
}

void Grille_g::dessinerTexteGrille()
{
    cout << "y" << endl
         << "^";
    for (int y = 0; y < nbColonne; y++)
    {
        cout << "[" << y << "] ";
    }
    cout << endl;

    for (int y = nbLigne - 1; y >= 0; y--)
    {
        cout << "| ";
        for (int x = 0; x < nbColonne; x++)
        {
            Pion_g *buff = (Pion_g *)getPion(x, y);
            if (buff != nullptr)
                cout << buff->idJoueur << " | ";
            else
                cout << " "
                     << " | ";
        }
        cout << endl;
    }
    cout << " ";
    for (int y = 0; y < nbColonne; y++)
    {
        cout << "[" << y << "] ";
    }

    cout << "x" << endl;
}

int signe(int val)
{
    if(val < 0) return -1;
    else return 1;
}

bool Grille_g::animerLesPions()
{
    bool finished = true;

    for (int i = 0; i < nbColonne; i++)
    {
        for (int j = 0; j < nbLigne; j++)
        {
            Pion_g *temp = (Pion_g *)getPion(i, j);
            long second = std::chrono::duration_cast<std::chrono::milliseconds>(10ms).count();
            float delta = ((float)TempsAnim::getTempsDelta()) / ((float)second);
            if (temp != nullptr)
            {

                if (abs(temp->posCible.x - temp->posAct.x) > abs(temp->vitesse * delta))
                {
                    int b = abs(temp->vitesse * delta);
                    finished = false;
                    int c = signe(temp->posCible.x - temp->posAct.x);
                    int d = c * b;
                    temp->posAct.x += d;

                    finished = false;
                }
                else
                {
                    temp->posAct.x = temp->posCible.x;
                }

                if (abs(temp->posCible.y - temp->posAct.y) > abs(temp->vitesse * delta))
                {
                    int b = abs(temp->vitesse * delta);
                    finished = false;
                    int c = signe(temp->posCible.y - temp->posAct.y);
                    int d = c * b;
                    temp->posAct.y += d;
                }
                else
                {
                    temp->posAct.y = temp->posCible.y;
                }
            }
        }
    }
    TempsAnim::init();
    dessinerGrille();
    return finished;
}

void Grille_g::dessinerGrille()
{
    Color C = Color(35, 84, 170);
    for (int i = 0; i < nbColonne; i++)
    {
        for (int j = 0; j < nbLigne; j++)
        {
            sf::CircleShape disque(rayon);
            Pion_g *temp = (Pion_g *)getPion(i, j);
            disque.setFillColor(Color::Green);
            if (temp != nullptr)
            {
                if (temp->idJoueur == 1)
                    disque.setFillColor(Color::Red);
                else
                    disque.setFillColor(Color::Yellow);

                disque.setPosition(temp->posAct.x, temp->posAct.y);
            }
            disque.setRotation(270);

            fenetre->draw(disque);
        }
    }

    for (int i = 0; i < nbColonne; i++)
    {
        for (int j = 0; j < nbLigne; j++)
        {

            CircleShape cercle(rayon);
            cercle.setFillColor(Color::Transparent);
            unsigned int y = (hauteurPX + pos.y) - dist / 2 - (j) * (dist + 2 * rayon);
            cercle.setPosition(pos.x + dist / 2 + i * (dist + 2 * rayon), y);
            cercle.setOutlineThickness(dist);
            cercle.setOutlineColor(C);
            cercle.setRotation(270);
            fenetre->draw(cercle);
        }
    }

    for (int i = 0; i <= nbColonne; i++)
    {
        RectangleShape colonne;
        colonne.setFillColor(C);
        colonne.setSize(Vector2f(dist, hauteurPX));
        colonne.setOrigin(dist / 2, 0);
        colonne.setPosition(pos.x + i * 2 * rayon + i * dist, pos.y);
        fenetre->draw(colonne);
    }

    for (int i = 0; i <= nbLigne; i++)
    {
        sf::RectangleShape ligne;
        ligne.setFillColor(C);
        ligne.setSize(Vector2f(largeurPX, dist));
        ligne.setOrigin(0, dist / 2);
        ligne.setPosition(pos.x, (hauteurPX + pos.y) - (i) * (dist + 2 * rayon));
        fenetre->draw(ligne);
    }

    float maxi = max(nbLigne, nbColonne) / 2;

    for (int i = 0; i <= nbColonne; i++)
    {
        for (int j = 0; j <= nbLigne; j++)
        {
            RectangleShape carre;
            carre.setFillColor(C);
            carre.setSize(Vector2f(maxi * dist, maxi * dist));
            carre.setOrigin(maxi / 2 * dist, maxi / 2 * dist);
            carre.setPosition(pos.x + i * (dist + 2 * rayon), (hauteurPX + pos.y) - j * (dist + 2 * rayon));
            carre.setRotation(45);
            fenetre->draw(carre);
        }
    }

    RectangleShape bord;
    bord.setFillColor(Color::Transparent);
    bord.setOutlineThickness(maxi * dist);
    bord.setOutlineColor(C);
    bord.setPosition(pos.x, (hauteurPX + pos.y) - (nbLigne) * (dist + 2 * rayon));
    bord.setSize(Vector2f(largeurPX - dist / 2, hauteurPX - dist / 2));
    fenetre->draw(bord);
}

int Grille_g::posPoint()
{
    Vector2u position = (Vector2u)Mouse::getPosition(*fenetre);

    int i = 1;
    if ((position.x >= pos.x) and (position.x < pos.x + largeurPX) and (position.y > 0) and (position.y < pos.y + hauteurPX))
    {
        while (!(position.x < pos.x + i * largeurPX / nbColonne) and (i <= nbColonne))
        {
            i++;
        }
        return i - 1;
    }
    else
    {
        return -1;
    }
}

void Grille_g::mettreAJourPositionCiblePion(Pion_g *pion)
{
    if (pion != 0x0)
    {
        pion->posCible.x = pos.x + dist / 2 + pion->posX * (dist + 2 * rayon);
        pion->posCible.y = (hauteurPX + pos.y) - dist / 2 - pion->posY * (dist + 2 * rayon);
    }
}

void Grille_g::mettreAJourPositionActuellePion(Pion_g *pion)
{
    if (pion != 0x0)
    {
        pion->posAct.x = pion->posCible.x;
        pion->posAct.y = pion->posCible.y;
    }
}

bool Grille_g::ajouterPion(const int joueur, int column, bool mid, bool UpDown)

{
    if ((column < nbColonne) and (column >= 0))
    {
        Pion_g *temp = new Pion_g(joueur);
        if (insererPion(column, temp, mid, UpDown))
        {
            temp->posCible.x = pos.x + dist / 2 + column * (dist + 2 * rayon);
            temp->posCible.y = (hauteurPX + pos.y) - dist / 2 - temp->posY * (dist + 2 * rayon);

            temp->posAct.x = temp->posCible.x;
            if (UpDown){
                //cout<< "Insertion par le haut ! " << endl;
               temp->posAct.y = pos.y;
            }
            else temp->posAct.y = pos.y + hauteurPX;
            return true;
        }
    }

    return false;
}

void Grille_g::decalageVertical(int direction, bool boucle)
{
    for (int y = 0; y < nbLigne; y++)
    {
        for (int x = 0; x < nbColonne; x++)
        {

            switch (direction)
            {
            case -1:
                decalageVectoriel(getPion(x, y), 0, direction, boucle);
                break;
            case 1:
                decalageVectoriel(getPion(x, nbLigne - 1 - y), 0, direction, boucle);
                break;
            }
            mettreAJourPositionCiblePion((Pion_g *)getPion(x, y));
        }
    }
    for (int x = 0; x < nbColonne; x++)
    {
        if (getPion(x, nbLigne) != 0x0)
        {
            ((Pion_g *)getPion(x, nbLigne))->posAct.x = ((Pion_g *)getPion(x, nbLigne))->posCible.x;
            ((Pion_g *)getPion(x, nbLigne))->posAct.y = pos.y;
        }

        decalageVectoriel(getPion(x, nbLigne), 0, direction);
    }

    for (int y = 0; y < nbLigne; y++)
    {
        for (int x = 0; x < nbColonne; x++)
        {
            mettreAJourPositionCiblePion((Pion_g *)getPion(x, y));
        }
    }
}

void Grille_g::decalageHorizontal(int direction, bool boucle)
{
    for (int x = 0; x < nbColonne; x++)
    {
        for (int y = 0; y < nbLigne; y++)
        {
            switch (direction)
            {
            case -1:
                decalageVectoriel(getPion(x, y), direction, 0, boucle);
                break;
            case 1:
                decalageVectoriel(getPion(nbColonne - 1 - x, y), direction, 0, boucle);
                break;
            }
        }
    }

    for (int y = 0; y < nbLigne; y++)
    {
        decalageVectoriel(getPion(nbColonne, y), direction, 0);
    }

    for (int x = 0; x < nbColonne; x++)
    {
        for (int y = 0; y < nbLigne; y++)
        {
            mettreAJourPositionCiblePion((Pion_g *)getPion(x, y));
        }
    }

    for (int y = 0; y < nbLigne; y++)
    {
        mettreAJourPositionCiblePion((Pion_g *)getPion(0, y));
        mettreAJourPositionActuellePion((Pion_g *)getPion(0, y));
    }
}

void Grille_g::rotaterAvecGravite(int angle)
{
    assert(angle == 90 || angle == 180 || angle == -90);

    int maxi = max(nbLigne, nbColonne);
    for (int x = 0; x < maxi; x++)
    {
        for (int y = 0; y < maxi; y++)
        {
            pions_buff[x][y] = nullptr;
        }
    }

    for (int x = 0; x < nbColonne; x++)
    {
        for (int y = 0; y < nbLigne; y++)
        {
            switch (angle)
            {
            case 90:
                deplacerPionDepuisJusquA(pions, pions_buff, x, y, nbLigne - 1 - y, x);
                break;
            case -90:
                deplacerPionDepuisJusquA(pions, pions_buff, x, y, y, nbColonne - 1 - x);
                break;
            case 180:
                deplacerPionDepuisJusquA(pions, pions_buff, x, y, nbColonne - 1 - x, nbLigne - 1 - y);
                break;
            }
        }
    }

    for (int x = 0; x < maxi; x++)
    {
        for (int y = 0; y < maxi; y++)
        {
            pions[x][y] = nullptr;
        }
    }

    for (int x = 0; x < maxi; x++)
    {
        for (int y = 0; y < maxi; y++)
        {
            deplacerPionDepuisJusquA(pions_buff, pions, x, y, x, y);
        }
    }

    int nbLigneOrigine = nbLigne;
    switch (angle)
    {
    case 90:

        nbLigne = nbColonne;
        nbColonne = nbLigneOrigine;

        break;
    case -90:

        nbLigne = nbColonne;
        nbColonne = nbLigneOrigine;
        break;
    }
    decalage(0, -1);
}

void Grille_g::rotaterSansGravite(int angle)
{
    assert(angle == 90 || angle == 180 || angle == -90);
    int maxi = max(nbLigne, nbColonne);
    for (int x = 0; x < maxi; x++)
    {
        for (int y = 0; y < maxi; y++)
        {
            pions_buff[x][y] = nullptr;
        }
    }

    for (int x = 0; x < nbColonne; x++)
    {
        for (int y = 0; y < nbLigne; y++)
        {
            switch (angle)
            {
            case 90:
                deplacerPionDepuisJusquA(pions, pions_buff, x, y, nbLigne - 1 - y, x);
                break;
            case -90:
                deplacerPionDepuisJusquA(pions, pions_buff, x, y, y, nbColonne - 1 - x);
                break;
            case 180:
                deplacerPionDepuisJusquA(pions, pions_buff, x, y, nbColonne - 1 - x, nbLigne - 1 - y);
                break;
            }
        }
    }

    for (int x = 0; x < maxi; x++)
    {
        for (int y = 0; y < maxi; y++)
        {
            pions[x][y] = nullptr;
        }
    }

    for (int x = 0; x < maxi; x++)
    {
        for (int y = 0; y < maxi; y++)
        {
            deplacerPionDepuisJusquA(pions_buff, pions, x, y, x, y);
        }
    }

    int nombreLigneOrigine = nbLigne;
    switch (angle)
    {
    case 90:

        nbLigne = nbColonne;
        nbColonne = nombreLigneOrigine;

        break;
    case -90:

        nbLigne = nbColonne;
        nbColonne = nombreLigneOrigine;
        break;
    }
}