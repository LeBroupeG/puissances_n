#ifndef GRILLE_G_H
#define GRILLE_G_H

#include "../core/Grille_c.h"
#include <SFML/Graphics.hpp>
#include <iostream>
#include "Pion_g.h"
#include "Bouton.h"
#include "TempsAnim.h"
#include <cassert>
#include <cmath>

using namespace sf;
using namespace std;

/**
 * @brief grille destinée à l'affichage graphique
 */
class Grille_g : public Grille_c
{
protected:
  RenderWindow *fenetre;
  float largeurPX;
  float hauteurPX;
  Vector2f pos;
  unsigned int rayon;
  unsigned int dist;

public:
  /**
  * @brief Constructeur de la Grille permettant de faire une grille de la taille que l'on désire
  * @param p_fenetre
  * @param largeur
  * @param hauteur
  */
  Grille_g(RenderWindow *p_fenetre, int largeur, int hauteur);

  /**
  * @brief permet de dessiner la grille et les pions sur la fenêtre
  * @return (void)
  */
  void dessinerGrille();
  /**
  * @brief dessine la grille dans le terminal (pour le débogage seulement)
  * @return (void)
  */
  void dessinerTexteGrille();
  /**
  * @brief anime les déplacements des pions dans la grille
  * @return bool : animations terminées ?
  */
  bool animerLesPions();

  /**
  * @brief cette fonction renvoie la colonne dans laquelle se trouvait le pointeur au moment du clic
  * @return entier : colonne dans laquelle se trouve le pointeur de la souris
  */
  int posPoint();

  /**
  * @brief Permet d'ajouter un pion dans la grille
  * @param joueur
  * @param column
  * @param mid
  * @param UpDown
  * @return bool : ajout réussi ?
  */
  bool ajouterPion(const int joueur, int column, bool mid, bool UpDown = true);

  /**
  * @brief pour l'animation, décale tous les pions à l'horizontale
  * @param int  : direction de l'ajout
  * @param boucle
  * @return (void)
  */
  void decalageHorizontal(int, bool boucle = false) override; /// Mode de jeu "Shift Horizontal"

  /**
     * @brief pour l'animation, décale tous les pions à la verticale
     * @param int  : direction de l'ajout
     * @param boucle
     * @return (void)
     */
  void decalageVertical(int, bool boucle = false) override; /// Mode de jeu "Shift Vertical"

  /**
     * @brief pour l'animation fait tourner la grille de l'angle (en degré, sens antihoraire) en paramètre, les pions retombent au centre
     * @param angle
     * @return (void)
     */
  void rotaterAvecGravite(int angle) override; /// Mode de jeu "Rotation classique"

  /**
     * @brief pour l'animation fait tourner la grille de l'angle (en degré, sens antihoraire) en paramètre, les pions ne bougent pas
     * @param angle
     * @return (void)
     */
  void rotaterSansGravite(int angle) override; /// Mode de jeu "Rotation par centre de gravité"

  /**
  * @brief met a jour la position cible d'un pion passé en paramètre
  * @param pion
  * @return (void)
  */
  void mettreAJourPositionCiblePion(Pion_g *pion);
  /**
  * @brief met a jour la position actuelle d'un pion passé en paramètre
  * @param pion
  * @return (void)
  */
  void mettreAJourPositionActuellePion(Pion_g *pion);
};

#endif