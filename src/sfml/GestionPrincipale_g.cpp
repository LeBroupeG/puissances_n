#include "GestionPrincipale_g.h"

GestionPrincipale_g::GestionPrincipale_g(RenderWindow *p_fenetre) : GestionPrincipale_c()
{
    fenetre = p_fenetre;
    etapeJeu = 0;
    etapeIntro = 0;
    initOk = false;
    enfonce = false;
    iAActivee = false;
    assert(pageSuivante.loadFromFile("content/sons/item1.wav"));
    assert(pagePrecedente.loadFromFile("content/sons/item.wav"));
    assert(clique.loadFromFile("content/sons/gui_click.wav"));
    assert(retourAccueil.loadFromFile("content/sons/bighit.wav"));

    policeTexte.loadFromFile("content/police/Roboto-Regular.ttf");
    for (int i = 0; i < 8; i++)
    {
        tabBouton[i] = nullptr;
    }
}

GestionPrincipale_g::~GestionPrincipale_g()
{
    if (partiePrincipale != nullptr)
    {
        delete partiePrincipale;
        partiePrincipale = nullptr;
    }

    for (int i = 0; i < 8; i++)
    {
        if (tabBouton[i] != nullptr)
        {
            delete tabBouton[i];
            tabBouton[i] = nullptr;
        }
    }
}

void GestionPrincipale_g::prendreEnMain()
{
    if (etapeJeu == 0 and fenetre->isOpen())
    {
        afficherEcranAccueil();

        //    TempsAnim().init();
    }
    else
    {
        if (etapeJeu == 1 && fenetre->isOpen())
            init();

        else
        {

            if (rgChainage < nbChainage and etapeJeu == 2 and fenetre->isOpen())
            {
                assert(partiePrincipale != nullptr);
                int gagnant = partiePrincipale->pendreEnMainJeu();
                boutonGrille();
                if (gagnant == 1)
                {
                    score1++;
                    rgChainage++;
                    etapeJeu++;
                    initOk = false;
                }
                else if (gagnant == 2)
                {
                    score2++;
                    rgChainage++;
                    etapeJeu++;
                    initOk = false;
                }
                else if (gagnant == 3)
                {
                    rgChainage++;
                    etapeJeu++;
                    initOk = false;
                }
            }
            else
            {
                if (etapeJeu == 3 and fenetre->isOpen())
                    ecrireScore();
                else if (etapeJeu == 4 and fenetre->isOpen())
                    afficherEcranFin();
            }
        }
    }
}

void GestionPrincipale_g::init()
{
    int nbBoutons = 0;

    fenetre->clear(Color(175, 175, 175)); //Color(11, 162, 11));
    Text Titre;
    setText(Titre, "Puissance 4", fenetre->getSize().x / 2, 60, 50);

    Text nameSelector;
    setText(nameSelector, " ", fenetre->getSize().x / 12, fenetre->getSize().y / 3, 30);

    switch (etapeIntro)
    {
        //première page du menu
    case 0:
        setText(nameSelector, "Combien de manches voulez-vous faire ?", fenetre->getSize().x / 2, fenetre->getSize().y / 3, 30);
        nbBoutons = 4;
        if (!initOk)
        {
            for (int i = 0; i < 8; i++)
            {
                if (tabBouton[i] != nullptr)
                {
                    delete tabBouton[i];
                    tabBouton[i] = nullptr;
                }
            }

            for (unsigned int i = 0; i < 3; i++)
            {
                tabBouton[i] = new Bouton(fenetre, Vector2f(fenetre->getSize().x * (i + 1) / 4, fenetre->getSize().y / 2), Vector2f(50, 50), to_string(1 + 2 * i));
            }

            tabBouton[3] = new Bouton(fenetre, Vector2f(fenetre->getSize().x / 2, fenetre->getSize().y - 60), Vector2f(40, 40), " ", "content/arrow.png");
            initOk = true;
        }

        for (int i = 1; i < 3; i++)
        {
            if (tabBouton[i - 1]->etat and tabBouton[i]->etat)
            {
                if (tabBouton[i - 1]->activation > tabBouton[i]->activation)
                    tabBouton[i]->etat = false;
                else
                    tabBouton[i - 1]->etat = false;
            }
        }

        if (tabBouton[0]->etat and tabBouton[2]->etat)
        {
            if (tabBouton[0]->activation > tabBouton[2]->activation)
                tabBouton[2]->etat = false;
            else
                tabBouton[0]->etat = false;
        }

        if (tabBouton[3]->etat)
        {
            for (int i = 0; i < 3; i++)
            {
                if (tabBouton[i]->etat)
                {
                    nbChainage = 1 + 2 * i;
                    estChainee = false;
                    etapeIntro++;
                    initOk = false;
                }
            }
            jouerSon(pageSuivante);
            tabBouton[3]->etat = false;
        }
        break;
    //deuxième page
    case 1:
        setText(nameSelector, "Quelle taille de grille voulez-vous ?", fenetre->getSize().x / 2, fenetre->getSize().y / 3, 30);
        nbBoutons = 6;
        if (!initOk)
        {
            for (int i = 0; i < 8; i++)
            {
                if (tabBouton[i] != nullptr)
                {
                    delete tabBouton[i];
                    tabBouton[i] = nullptr;
                }
            }

            tabBouton[0] = new Bouton(fenetre, Vector2f(fenetre->getSize().x * 1 / 5, fenetre->getSize().y / 2), Vector2f(130, 60), "Petit");
            tabBouton[1] = new Bouton(fenetre, Vector2f(fenetre->getSize().x * 2 / 5, fenetre->getSize().y / 2), Vector2f(130, 60), "Standard");
            tabBouton[2] = new Bouton(fenetre, Vector2f(fenetre->getSize().x * 3 / 5, fenetre->getSize().y / 2), Vector2f(130, 60), "Grand");
            tabBouton[3] = new Bouton(fenetre, Vector2f(fenetre->getSize().x * 4 / 5, fenetre->getSize().y / 2), Vector2f(130, 60), "Aleatoire");
            tabBouton[4] = new Bouton(fenetre, Vector2f(fenetre->getSize().x * 1 / 3, fenetre->getSize().y - 60), Vector2f(40, 40), " ", "content/arrow.png");
            tabBouton[4]->setRotation(180);
            tabBouton[4]->setPosition(Vector2f(fenetre->getSize().x * 1 / 3, fenetre->getSize().y - 60));

            tabBouton[5] = new Bouton(fenetre, Vector2f(fenetre->getSize().x * 2 / 3, fenetre->getSize().y - 60), Vector2f(40, 40), " ", "content/arrow.png");

            initOk = true;
        }

        for (int i = 1; i < 4; i++)
        {
            if (tabBouton[i - 1]->etat and tabBouton[i]->etat)
            {
                if (tabBouton[i - 1]->activation > tabBouton[i]->activation)
                    tabBouton[i]->etat = false;
                else
                    tabBouton[i - 1]->etat = false;
            }
        }
        for (int i = 2; i < 4; i++)
        {
            if (tabBouton[i]->etat and tabBouton[i - 2]->etat)
            {
                if (tabBouton[i - 2]->activation > tabBouton[i]->activation)
                    tabBouton[i]->etat = false;
                else
                    tabBouton[i - 2]->etat = false;
            }
        }

        if (tabBouton[3]->etat and tabBouton[0]->etat)
        {
            if (tabBouton[0]->activation > tabBouton[3]->activation)
                tabBouton[3]->etat = false;
            else
                tabBouton[0]->etat = false;
        }

        if (tabBouton[5]->etat)
        {
            for (int i = 0; i < 3; i++)
            {
                if (tabBouton[i]->etat)
                {
                    largeur = 6 + i;
                    hauteur = 5 + i;
                    etapeIntro++;
                    initOk = false;
                }
            }
            if (tabBouton[3]->etat)
            {
                int alea = rand() % 2;
                largeur = 6 + alea;
                hauteur = 5 + alea;
                modeDeJeuAleaChainage = true;
                etapeIntro++;
                initOk = false;
            }
            jouerSon(pageSuivante);
            tabBouton[5]->etat = false;
        }

        if (tabBouton[4]->etat)
        {
            etapeIntro--;
            initOk = false;
            jouerSon(pagePrecedente);
        }

        break;
    //troisième page
    case 2:
        setText(nameSelector, "Voulez-vous jouez contre l'IA ?", fenetre->getSize().x / 2, fenetre->getSize().y / 3, 30);
        nbBoutons = 5;
        if (!initOk)
        {
            iAActivee = false;
            for (int i = 0; i < 8; i++)
            {
                if (tabBouton[i] != nullptr)
                {
                    delete tabBouton[i];
                    tabBouton[i] = nullptr;
                }
            }

            for (int i = 0; i < 3; i++)
            {
                tabBouton[i] = new Bouton(fenetre, Vector2f(fenetre->getSize().x * (i + 1) / 4, fenetre->getSize().y / 2), Vector2f(120, 60), "Niveau " + to_string(i + 1));
            }
            tabBouton[3] = new Bouton(fenetre, Vector2f(fenetre->getSize().x * 1 / 3, fenetre->getSize().y - 60), Vector2f(40, 40), " ", "content/arrow.png");
            tabBouton[3]->setRotation(180);
            tabBouton[3]->setPosition(Vector2f(fenetre->getSize().x * 1 / 3, fenetre->getSize().y - 60));

            tabBouton[4] = new Bouton(fenetre, Vector2f(fenetre->getSize().x * 2 / 3, fenetre->getSize().y - 60), Vector2f(40, 40), " ", "content/arrow.png");

            initOk = true;
        }

        for (int i = 1; i < 3; i++)
        {
            if (tabBouton[i - 1]->etat and tabBouton[i]->etat)
            {
                if (tabBouton[i - 1]->activation > tabBouton[i]->activation)
                    tabBouton[i]->etat = false;
                else
                    tabBouton[i - 1]->etat = false;
            }
        }

        if (tabBouton[0]->etat and tabBouton[2]->etat)
        {
            if (tabBouton[0]->activation > tabBouton[2]->activation)
                tabBouton[2]->etat = false;
            else
                tabBouton[0]->etat = false;
        }

        if (tabBouton[4]->etat)
        {
            for (int i = 0; i < 3; i++)
            {
                if (tabBouton[i]->etat)
                {
                    iAActivee = true;
                    niveauIA = i + 1;
                }
            }
            etapeIntro++;
            initOk = false;
            jouerSon(pageSuivante);
            tabBouton[4]->etat = false;
        }

        if (tabBouton[3]->etat)
        {
            etapeIntro--;
            initOk = false;
            jouerSon(pagePrecedente);
        }

        break;

    case 3:
        setText(nameSelector, "Voulez-vous mettre une limite de temps pour jouer ?", fenetre->getSize().x / 2, fenetre->getSize().y / 3, 30);
        nbBoutons = 5;
        if (!initOk)
        {
            for (int i = 0; i < 8; i++)
            {
                if (tabBouton[i] != nullptr)
                {
                    delete tabBouton[i];
                    tabBouton[i] = nullptr;
                }
            }

            for (unsigned int i = 0; i < 3; i++)
            {
                tabBouton[i] = new Bouton(fenetre, Vector2f(fenetre->getSize().x * (i + 1) / 4, fenetre->getSize().y / 2), Vector2f(100, 50), to_string(1 + i) + "0 sec");
            }

            tabBouton[3] = new Bouton(fenetre, Vector2f(fenetre->getSize().x / 2, fenetre->getSize().y - 60), Vector2f(40, 40), " ", "content/arrow.png");
            tabBouton[3]->setRotation(180);
            tabBouton[3]->setPosition(Vector2f(fenetre->getSize().x * 1 / 3, fenetre->getSize().y - 60));

            tabBouton[4] = new Bouton(fenetre, Vector2f(fenetre->getSize().x * 2 / 3, fenetre->getSize().y - 60), Vector2f(40, 40), " ", "content/arrow.png");
            initOk = true;
        }

        for (int i = 1; i < 3; i++)
        {
            if (tabBouton[i - 1]->etat and tabBouton[i]->etat)
            {
                if (tabBouton[i - 1]->activation > tabBouton[i]->activation)
                    tabBouton[i]->etat = false;
                else
                    tabBouton[i - 1]->etat = false;
            }
        }

        if (tabBouton[0]->etat and tabBouton[2]->etat)
        {
            if (tabBouton[0]->activation > tabBouton[2]->activation)
                tabBouton[2]->etat = false;
            else
                tabBouton[0]->etat = false;
        }

        if (tabBouton[4]->etat)
        {
            tempsLimitAleaChainage = false;
            for (int i = 0; i < 3; i++)
            {
                if (tabBouton[i]->etat)
                {
                    tempsLimitAleaChainage = true;
                    tempsLimite = (i + 1) * 10;
                }
            }
            etapeIntro++;
            initOk = false;
            jouerSon(pageSuivante);
            tabBouton[3]->etat = false;
        }

        if (tabBouton[3]->etat)
        {
            etapeIntro--;
            initOk = false;
            jouerSon(pagePrecedente);
        }

        break;

    //dernière page
    case 4:
        setText(nameSelector, "Quelle Mode de jeu voulez-vous ?", fenetre->getSize().x / 2, fenetre->getSize().y / 4, 30);
        nbBoutons = 7;
        if (!initOk)
        {
            for (int i = 0; i < 8; i++)
            {
                if (tabBouton[i] != nullptr)
                {
                    delete tabBouton[i];
                    tabBouton[i] = nullptr;
                }
            }
            int largeurBouton = fenetre->getSize().x / 2;
            int posBouton = 9 * fenetre->getSize().y / 24 + 15;
            tabBouton[0] = new Bouton(fenetre, Vector2f(fenetre->getSize().x / 2, posBouton), Vector2f(largeurBouton, 50), to_string(1) + ". Partie Normale");
            tabBouton[1] = new Bouton(fenetre, Vector2f(fenetre->getSize().x / 2, posBouton + 60), Vector2f(largeurBouton, 50), to_string(2) + ". Shifting vertical");
            tabBouton[2] = new Bouton(fenetre, Vector2f(fenetre->getSize().x / 2, posBouton + 120), Vector2f(largeurBouton, 50), to_string(3) + ". Shifting horizontal");
            tabBouton[3] = new Bouton(fenetre, Vector2f(fenetre->getSize().x / 2, posBouton + 180), Vector2f(largeurBouton, 50), to_string(4) + ". Mode insertion alternee");
            tabBouton[4] = new Bouton(fenetre, Vector2f(fenetre->getSize().x / 2, posBouton + 240), Vector2f(largeurBouton, 50), to_string(5) + ". Mode Gravite changeante");
            tabBouton[5] = new Bouton(fenetre, Vector2f(fenetre->getSize().x / 2, fenetre->getSize().y - 60), Vector2f(40, 40), " ", "content/arrow.png");
            tabBouton[5]->setRotation(180);
            tabBouton[5]->setPosition(Vector2f(fenetre->getSize().x * 1 / 3, fenetre->getSize().y - 60));

            tabBouton[6] = new Bouton(fenetre, Vector2f(fenetre->getSize().x * 2 / 3, fenetre->getSize().y - 60), Vector2f(100, 40), "Lancer");

            initOk = true;
        }

        for (int i = 1; i < 5; i++)
        {
            if (tabBouton[i - 1]->etat and tabBouton[i]->etat)
            {
                if (tabBouton[i - 1]->activation > tabBouton[i]->activation)
                    tabBouton[i]->etat = false;
                else
                    tabBouton[i - 1]->etat = false;
            }
        }
        for (int i = 2; i < 5; i++)
        {
            if (tabBouton[i]->etat and tabBouton[i - 2]->etat)
            {
                if (tabBouton[i - 2]->activation > tabBouton[i]->activation)
                    tabBouton[i]->etat = false;
                else
                    tabBouton[i - 2]->etat = false;
            }
        }

        for (int i = 3; i < 5; i++)
        {
            if (tabBouton[i]->etat and tabBouton[i - 3]->etat)
            {
                if (tabBouton[i - 3]->activation > tabBouton[i]->activation)
                    tabBouton[i]->etat = false;
                else
                    tabBouton[i - 3]->etat = false;
            }
        }

        for (int i = 4; i < 5; i++)
        {
            if (tabBouton[i]->etat and tabBouton[i - 4]->etat)
            {
                if (tabBouton[i - 4]->activation > tabBouton[i]->activation)
                    tabBouton[i]->etat = false;
                else
                    tabBouton[i - 4]->etat = false;
            }
        }

        if (tabBouton[4]->etat and tabBouton[0]->etat)
        {
            if (tabBouton[0]->activation > tabBouton[4]->activation)
                tabBouton[4]->etat = false;
            else
                tabBouton[0]->etat = false;
        }

        if (tabBouton[6]->etat)
        {
            for (int i = 0; i < 5; i++)
            {
                if (tabBouton[i]->etat)
                {
                    modeDeJeu = i;
                    etapeIntro++;
                    initOk = false;
                }
            }
            jouerSon(pageSuivante);

            tabBouton[6]->etat = false;
        }

        if (tabBouton[5]->etat)
        {
            nettoyerEcran();
            etapeIntro--;
            initOk = false;
            jouerSon(pagePrecedente);
        }

        break;

    default:
        etapeJeu++;
        nettoyerEcran();
        creerSession();
        break;
    }

    if (etapeIntro < 5)
    {
        if (Mouse::isButtonPressed(Mouse::Left) and !enfonce)
        {
            for (int i = 0; i < nbBoutons; i++)
            {
                int nbBoutonsSons;
                if (etapeIntro == 0)
                    nbBoutonsSons = nbBoutons - 1;
                else
                    nbBoutonsSons = nbBoutons - 2;

                if (tabBouton[i]->rafraichirBouton() and (i < nbBoutonsSons))
                    jouerSon(clique);
            }
            enfonce = true;
        }
        else
        {
            if (!(Mouse::isButtonPressed(Mouse::Left)) and enfonce)
            {
                enfonce = false;
            }
        }
        for (int i = 0; i < nbBoutons; i++)
        {

            tabBouton[i]->dessiner();
        }

        fenetre->draw(Titre);
        fenetre->draw(nameSelector);
    }
}

void GestionPrincipale_g::creerSession()
{

    partiePrincipale = new Partie_g(fenetre, modeDeJeu, largeur, hauteur, tempsLimitAleaChainage, tempsLimite, iAActivee, niveauIA);

    assert(partiePrincipale != nullptr);
}

void GestionPrincipale_g::nettoyerEcran()
{
    fenetre->clear(Color(125, 125, 125));
}

void GestionPrincipale_g::afficherEcranAccueil()
{
    if (!initOk)
    {
        tempsIntro = time(NULL);
        initOk = true;
    }
    if (difftime(time(NULL), tempsIntro) < 3)
    {
        fenetre->clear(Color(175, 175, 175));
        Text titre;
        Text auteurs;
        setText(titre, "Puissance 4", fenetre->getSize().x / 2, fenetre->getSize().y / 4, 80);
        setText(auteurs, "Par Noe Dubois, Tim Kosak et Romain Genty", fenetre->getSize().x / 2, fenetre->getSize().y * 3 / 4, 40);
        fenetre->draw(titre);
        fenetre->draw(auteurs);
    }
    else
    {
        initOk = false;
        etapeJeu++;
    }
}
void GestionPrincipale_g::ecrireScore()
{
    nettoyerEcran();
    if (partiePrincipale != nullptr)
        partiePrincipale->dessinerGrille();

    RectangleShape rectangle;
    rectangle.setFillColor(Color(175, 175, 175, 245));
    rectangle.setSize(Vector2f(fenetre->getSize().x, fenetre->getSize().y / 2));
    rectangle.setPosition(0, fenetre->getSize().y / 4);
    fenetre->draw(rectangle);
    Text scoreActuelle;

    if (iAActivee)
    {
        if (partiePrincipale->testerVictoire() == 1)
            setText(scoreActuelle, "Le gagnant de cette partie est l'intelligence artificielle", fenetre->getSize().x / 2, fenetre->getSize().y * 6 / 16, 40);
        else
            setText(scoreActuelle, "Le gagnant de cette partie est le joueur", fenetre->getSize().x / 2, fenetre->getSize().y * 6 / 16, 40);
    }
    else if(partiePrincipale->testerVictoire() == 1 or partiePrincipale->testerVictoire() == 2)
    {
        setText(scoreActuelle, "Le gagnant de cette partie est le joueur " + to_string(partiePrincipale->testerVictoire()), fenetre->getSize().x / 2, fenetre->getSize().y * 6 / 16, 40);
    }else
         setText(scoreActuelle, "Egalite", fenetre->getSize().x / 2, fenetre->getSize().y * 6 / 16, 40);


    fenetre->draw(scoreActuelle);

    switch (rgChainage < nbChainage)
    {
    case true:
        if (!initOk)
        {
            for (int i = 0; i < 8; i++)
            {
                if (tabBouton[i] != nullptr)
                {
                    delete tabBouton[i];
                    tabBouton[i] = nullptr;
                }
            }
            tabBouton[0] = new Bouton(fenetre, Vector2f(fenetre->getSize().x / 2, fenetre->getSize().y * 3 / 4 - 60), Vector2f(100, 40), "continuer");
            initOk = true;
        }
        if (tabBouton[0]->etat)
        {
            if (partiePrincipale != nullptr)
            {
                delete partiePrincipale;
                partiePrincipale = nullptr;
            }
            creerSession();
            etapeIntro = 5;
            etapeJeu = 2;
            delete tabBouton[0];
            tabBouton[0] = nullptr;
            initOk = false;
        }

        break;

    case false:
        if (!initOk)
        {
            for (int i = 0; i < 8; i++)
            {
                if (tabBouton[i] != nullptr)
                {
                    delete tabBouton[i];
                    tabBouton[i] = nullptr;
                }
            }
            tabBouton[0] = new Bouton(fenetre, Vector2f(fenetre->getSize().x / 2 - 60, fenetre->getSize().y * 3 / 4 - 60), Vector2f(100, 40), "rejouer");
            tabBouton[1] = new Bouton(fenetre, Vector2f(fenetre->getSize().x / 2 + 60, fenetre->getSize().y * 3 / 4 - 60), Vector2f(100, 40), "Quitter");
            initOk = true;
        }

        if (tabBouton[0]->etat)
        {
            score1 = 0;
            score2 = 0;
            scoreIA = 0;
            etapeJeu = 1;
            etapeIntro = 0;
            rgChainage = 0;
            initOk = false;
            if (partiePrincipale != nullptr)
            {
                delete partiePrincipale;
                partiePrincipale = nullptr;
            }
        }
        if (tabBouton[1]->etat)
        {
            etapeJeu++;
            initOk = false;
        }
        if (nbChainage > 1)
        {
            Text vainqueurTournoi;

            if (score1 > score2)
            {
                if (iAActivee)
                    setText(vainqueurTournoi, "Le vainqueur du tournoi est l'IA", fenetre->getSize().x / 2, fenetre->getSize().y * 9 / 16, 40);
                else
                    setText(vainqueurTournoi, "Le vainqueur du tournoi est le joueur 1", fenetre->getSize().x / 2, fenetre->getSize().y * 9 / 16, 40);
            }
            else if (score1 < score2)
                setText(vainqueurTournoi, "Le vainqueur du tournoi est le joueur 2", fenetre->getSize().x / 2, fenetre->getSize().y * 9 / 16, 40);
            else
                setText(vainqueurTournoi, "Pas de vainqueur ni de perdant sur ce tournoi", fenetre->getSize().x / 2, fenetre->getSize().y * 9 / 16, 40);

            fenetre->draw(vainqueurTournoi);
        }

        break;
    }

    Text score;
    setText(score, "Score : " + to_string(score1) + " - " + to_string(score2), fenetre->getSize().x / 2, fenetre->getSize().y * 15 / 32, 40);
    fenetre->draw(score);

    int nbBoutons = 0;
    for (int i = 0; i < 8; i++)
    {
        if (tabBouton[i] != nullptr)
        {
            tabBouton[i]->dessiner();
            nbBoutons = i + 1;
        }
    }

    if (Mouse::isButtonPressed(Mouse::Left) and !enfonce)
    {
        for (int i = 0; i < nbBoutons; i++)
        {
            int nbBoutonsSons;
            if (rgChainage < nbChainage)
                nbBoutonsSons = 1;
            else
                nbBoutonsSons = 2;

            if (tabBouton[i]->rafraichirBouton() and (i < nbBoutonsSons))
                jouerSon(clique);
        }
        enfonce = true;
    }
    else
    {
        if (!(Mouse::isButtonPressed(Mouse::Left)) and enfonce)
        {
            enfonce = false;
        }
    }
}

void GestionPrincipale_g::afficherEcranFin()
{
    if (!initOk)
    {
        tempsIntro = time(NULL);
        initOk = true;
    }
    if (difftime(time(NULL), tempsIntro) < 5)
    {
        fenetre->clear(Color(175, 175, 175));
        Text titre;
        Text auteurs;
        setText(titre, "Merci d'avoir joue au Puissance 4", fenetre->getSize().x / 2, fenetre->getSize().y / 4, 80);
        setText(auteurs, "Par Noe Dubois, Tim Kosak et Romain Genty", fenetre->getSize().x / 2, fenetre->getSize().y * 3 / 4, 40);
        fenetre->draw(titre);
        fenetre->draw(auteurs);
    }
    else
    {
        fenetre->close();
    }
}

void GestionPrincipale_g::setText(Text &T, string charac, unsigned int posX, unsigned int posY, unsigned int Taille, Color C)
{
    T.setString(charac);
    T.setFont(policeTexte);
    T.setCharacterSize(Taille);
    T.setFillColor(C);
    FloatRect encadrement = T.getGlobalBounds();
    T.setOrigin(encadrement.width / 2, encadrement.height / 2);
    T.setPosition(posX, posY);
}

void GestionPrincipale_g::boutonGrille()
{
    if (!initOk)
    {
        for (int i = 0; i < 8; i++)
        {
            if (tabBouton[i] != nullptr)
            {
                delete tabBouton[i];
                tabBouton[i] = nullptr;
            }
        }
        tabBouton[0] = new Bouton(fenetre, Vector2f(fenetre->getSize().x - 30, 30), Vector2f(40, 40), " ", "content/home.png");
        initOk = true;
    }
    if (tabBouton[0]->etat)
    {
        etapeJeu = 1;
        initOk = false;
        delete partiePrincipale;
        partiePrincipale = nullptr;
        jouerSon(retourAccueil);
        etapeIntro = 0;
    }
    tabBouton[0]->setFillColor(Color::White);
    tabBouton[0]->dessiner();

    if (Mouse::isButtonPressed(Mouse::Left) and !enfonce)
    {
        tabBouton[0]->rafraichirBouton();
        enfonce = true;
    }
    else if (!(Mouse::isButtonPressed(Mouse::Left)) and enfonce)
    {
        enfonce = false;
    }
}

void GestionPrincipale_g::jouerSon(SoundBuffer &sonTemp)
{
    son.setBuffer(sonTemp);
    son.play();
}