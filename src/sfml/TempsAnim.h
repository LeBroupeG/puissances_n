#ifndef TPS_ANIM_G
#define TPS_ANIM_G

#include <chrono>
#include <time.h>

using namespace std;
using namespace std::chrono;

/**
 * @brief classe statique permettant de calculer les durées pour les animations
 */
class TempsAnim
{
private:
public:
	//static std::chrono::milliseconds tempsAbsFrame;
	/**
  * @brief intitialise la classe
  * @return (void)
  */
	static void init();

	/**
  * @brief calculer la durée entre deux images affichées à l'écran
  * @return (void)
  */
	static void calculerTempsDelta();

	static long tempsAbsFrame;

	/**
  * @brief renvoie le temps entre deux images affichées à l'écran
  * @return long
  */
	static long getTempsDelta();
};

#endif