#include <SFML/Graphics.hpp>
#include "GestionPrincipale_g.h"
using namespace sf;
using namespace std;

int main()
{
    sf::ContextSettings settings;

    settings.antialiasingLevel = 8;
    sf::RenderWindow  * fenetre = new RenderWindow(sf::VideoMode(1280, 720), "Puissance 4", sf::Style::Close, settings);
    Image icone;
    icone.loadFromFile("content/logo.png");
    fenetre->setFramerateLimit(75);
    fenetre->setIcon(50, 50, icone.getPixelsPtr());
    GestionPrincipale_g gestionnaire(fenetre);
    while (fenetre->isOpen())
    {
        sf::Event event;
        while (fenetre->pollEvent(event))
        {
            if (event.type == sf::Event::Closed or sf::Keyboard::isKeyPressed(Keyboard::Escape))
                fenetre->close();
        }

        gestionnaire.prendreEnMain();
        //fenetre->clear(Color::White);
        fenetre->display();
    }
    delete fenetre;

    return 0;
}