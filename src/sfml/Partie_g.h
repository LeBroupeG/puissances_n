#ifndef PARTIE_G
#define PARTIE_G

#include "Grille_g.h"
#include "../core/Partie_c.h"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <cassert>
#include <time.h>
#include <cstring>
#include "TempsAnim.h"

using namespace std;

/**
 * @brief Partie destinée à l'affichage graphique
 */
class Partie_g : public Partie_c
{
private:
    RenderWindow *p_fenetre;
    bool enfonce;
    bool initTemps = false;
    double debutCoup;
    double diffTemps;
    Text infoPartie;
    Font police;
    SoundBuffer ajout;
    Sound son;

public:
    bool animationEnCours = false;

    /**
     * @brief constructeur permettant de créer une partie avec les paramètres demandés par l'utilisateur
     * @param fenetre
     * @param modeJeu
     * @param largeurGrille_
     * @param hauteurGrille_
     * @param estTempsLimite
     * @param tempsLimite
     * @param iAActive_
     * @param niveauIA_
     */
    Partie_g(RenderWindow *fenetre, int modeJeu,
             int largeurGrille_, int hauteurGrille_,
             bool estTempsLimite, float tempsLimite, bool iAActive_, int niveauIA_ = 1);

    /**
     * @brief cette foncction gère les animations de la partie
     * @return (void)
     */
    void animation();

    /**
     * @brief cette fonction affiche la grille de la partie en cours
     * @return 
     */
    virtual void dessinerGrille();
    /**
     * @brief cette fonction permet de nettoyer la fenêtre pour dessiner dessus à nouveau
     * @return (void)
     */
    void nettoyerEcran();
    /**
     * @brief Permet d'écrire le joueur qui doit jouer et la limite de temps en haut à droite de la fenêtre
     * @return (void)
     */
    void ecrireDetails();
    /**
     * @brief Permet de savoir si un joueur à gagner la partie ou non
     * @return 
     */
    int testerVictoire();
    /**
     * @brief Permet de faire jouer un joueur puis l'autre tour à tour
     * @return (void)
     */
    void faireJouerLesJoueurs();
    /**
     * @brief cette fonction gère la partie en cours et utilise les fonctions précédentes pour faire évoluer la partie
     * @return 
     */
    int pendreEnMainJeu();
};

#endif