#ifndef GESTIONPRINCIPALE_G_H
#define GESTIONPRINCIPALE_G_H

#include "../core/GestionPrincipale_c.h"
#include "Partie_g.h"
#include "Bouton.h"
#include <string>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <time.h>
//#include "TempsAnim.h"

using namespace sf;
using namespace std;

/**
 * @brief Classe permettant de gérer tout le jeu en version graphique (choix des paramètres, création et enchaînemnt d epartie, fin de jeu...)
 */
class GestionPrincipale_g : public GestionPrincipale_c
{
    private:
        Bouton* tabBouton[8];
        Font policeTexte;
        int modeDeJeu, largeur, hauteur, niveauIA;
        float tempsLimite;
        unsigned int etapeIntro;
        unsigned int etapeJeu;
        bool enfonce, initOk;
        double tempsIntro;
        SoundBuffer pageSuivante, clique, pagePrecedente, retourAccueil;
        Sound son;

    public:
    RenderWindow *fenetre;

    /**
     * @brief Constructeur de gestionnaire de jeu
     * @param p_fenetre
     */
    GestionPrincipale_g(RenderWindow* p_fenetre);
    /**
     * @brief destructeur
     */
    ~GestionPrincipale_g();

    /**
     * @brief Affiche un menu à l'écran permettant de sélectionner les paramètres des prochaines parties
     * @return (void)
     */
    void init();
    /**
     * @brief Créer une partie avec les paramètres spécifiés dans le menu
     * @return (void)
     */
    void creerSession();

    /**
     * @brief Fonction permettant de faire fonctionner tout le jeu
     * @return (void)
     */
    void prendreEnMain();
    /**
     * @brief nettoie la fenêtre
     * @return (void)
     */
    void nettoyerEcran();
    /**
     * @brief affiche pendant 3 secondes l'écran avec le nom du jeu et les noms des auteurs du programme
     * @return (void)
     */
    void afficherEcranAccueil();
    /**
     * @brief affiche pendant 10 secs un message de remrciement d'avoir joué au jeu
     * @return (void)
     */
    void afficherEcranFin();
    /**
     * @brief permet d'afficher un menu avec le score des parties déjà faites et des boutons
     * @return (void)
     */
    void ecrireScore();
    /**
     * @brief affiche un bouton en haut à gauche de la grille pour permettre à l'utilisateur de revenir au menu en cours de partie
     * @return (void)
     */
    void boutonGrille();

    /**
     * @brief met un texte sfml en forme
     * @param Text de sfml
     * @param charac
     * @param int
     * @param int
     * @param int
     * @param C
     * @return (void)
     */
    void setText(Text &, string charac, unsigned int posX, unsigned int posY, unsigned int Taille = 60, Color C = Color::White);

    /**
     * @brief Joue le son passé en paramètre
     * @param sf:SoundBuffer
     * @return (void)
     */
    void jouerSon(SoundBuffer &);
};
#endif