#include "TempsAnim.h"

long TempsAnim::tempsAbsFrame = std::chrono::duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();

void TempsAnim::init()
{
	tempsAbsFrame = std::chrono::duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

void TempsAnim::calculerTempsDelta()
{

	tempsAbsFrame = std::chrono::duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

long TempsAnim::getTempsDelta()
{
	return std::chrono::duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count() - tempsAbsFrame;
}