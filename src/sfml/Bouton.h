#ifndef BUTTON
#define BUTTON

#include <SFML/Graphics.hpp>
#include <SFML/Graphics/RoundedRectangleShape.hpp>
#include <string>
#include <iostream>
#include <ctime>
using namespace sf;
using namespace std;

/** 
 * @class Classe pour créer des boutons dans les menus
 * **/
class Bouton : public RoundedRectangleShape
{
public:
    RenderWindow *p_fenetre;
    bool etat;
    Color Cenfonce; ///couleur du bouton activé
    Color Cnormal;  ///couleur dans son état normal
    Text nom;      ///texte affiché sur le bouton
    Font police;
    Texture imageButton;
    Image buttonImg;
    bool avecImage;
    clock_t activation;


    /**
     * @brief ce constructeur prend les attributs du bouton sous forme de vecteurs 2D 
     * @param fenetre
     * @param pos
     * @param size
     * @param nom
     * @param cheminImage
     */
    Bouton(RenderWindow *fenetre, Vector2f pos, Vector2f size, string nom = " ", string cheminImage = "NULL");

    /**
     * @brief permet de changer l'état du bouton en cours de programme
     * @return 
     */
    bool rafraichirBouton();

    /**
     * @brief permet de savoir si le pointeur de la souris est sur le bouton ou non
     * @return 
     */
    bool enfonce();

    /**
     * @brief permet de dessiner le bouton sur la fenêtre active
     * @return (void)
     */
    void dessiner();
};

#endif