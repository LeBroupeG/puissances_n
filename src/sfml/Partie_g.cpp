#include "Partie_g.h"

Partie_g::Partie_g(RenderWindow *fenetre, int modeJeu,
				   int largeurGrille_, int hauteurGrille_,
				   bool estTempsLimite_, float tempsLimite_, bool iAActive_, int niveauIA_)
	: Partie_c(modeJeu, largeurGrille_, hauteurGrille_, estTempsLimite_, tempsLimite_, iAActive_, niveauIA_)
{

	p_fenetre = fenetre;
	enfonce = true;
	police.loadFromFile("content/police/Roboto-Regular.ttf");
	ajout.loadFromFile("content/sons/select2.wav");
	if (!(largeurGrille_ <= 4 or hauteurGrille_ <= 4))
	{

		switch (modeJeu)
		{
		case 0:
			grille = new Grille_g(p_fenetre, largeurGrille_, hauteurGrille_);
			break;
		case 5:
			grille = new Grille_g(p_fenetre, largeurGrille_, hauteurGrille_);
			break;
		case 1:
			grille = new Grille_g(p_fenetre, largeurGrille_, hauteurGrille_);
			break;
		case 2:
			grille = new Grille_g(p_fenetre, largeurGrille_, hauteurGrille_);
			break;
		case 3:
			if ((hauteurGrille_ % 2) == 0)
			{
				grille = new Grille_g(p_fenetre, largeurGrille_, hauteurGrille_ + 1);
			}
			else
			{
				grille = new Grille_g(p_fenetre, largeurGrille_, hauteurGrille_);
			}
			break;
		case 4:
			unsigned int h, t;
			h = hauteurGrille_;
			t = largeurGrille_;
			if (t < h)
			{
				if (h % 2 == 0)
				{
					t = h + 1;
					h++;
				}
				else
					t = h;
			}
			else
			{
				if (t % 2 == 0)
					t++;
				h = t;
			}
			grille = new Grille_g(p_fenetre, t, h);
			break;
		}
	}
	assert(grille != nullptr);

	if (iAActive)
		intArt->initMCTS();
}

void Partie_g::dessinerGrille()
{
	grille->dessinerGrille();
}

void Partie_g::nettoyerEcran()
{
	p_fenetre->clear();
}

void Partie_g::ecrireDetails()
{
	infoPartie.setPosition(10, 10);
	infoPartie.setCharacterSize(24);
	infoPartie.setFillColor(Color::Black);
	infoPartie.setFont(police);
	string Temps = " ";

	infoPartie.setString("Joueur " + to_string(tourJoueur1 ? 1 : 2));
	p_fenetre->draw(infoPartie);

	if (tempsLimiteActif)
	{
		infoPartie.setString("a " + to_string((int)(tempsLimite - diffTemps)) + " secs pour jouer");
		infoPartie.setPosition(10, 40);
		p_fenetre->draw(infoPartie);
	}
}

int Partie_g::testerVictoire()
{

	if (grille->unJoueurAGagne(4, 1))
	{
		return 1;
	}
	else if (grille->unJoueurAGagne(4, 2))
	{
		return 2;
	}
	else
	{
		return 0;
	}
}

void Partie_g::faireJouerLesJoueurs()
{
	if (!initTemps)
	{
		debutCoup = time(NULL);
		initTemps = true;
	}
	diffTemps = difftime(time(NULL), debutCoup);

	if (tourJoueur1 && iAActive)
	{
		Grille_g *grilleTemp = (Grille_g *)grille;
		grilleTemp->ajouterPion(1, intArt->jouer(), tomberAuMilieu(), insererPionsParDessus);
		tourJoueur1 = !tourJoueur1;
		leJoueurAJoue = true;
		son.setBuffer(ajout);
		son.play();
		initTemps = false;
	}
	else if (Mouse::isButtonPressed(Mouse::Left) and !enfonce and (!(tempsLimiteActif) or tempsLimite - diffTemps > 0))
	{
		int a = grille->posPoint();
		Grille_g *grilleTemp = (Grille_g *)grille;
		if (grilleTemp->ajouterPion(tourJoueur1 ? 1 : 2, a, tomberAuMilieu(), insererPionsParDessus))
		{
			if (!tourJoueur1 && iAActive)
			{
				intArt->lAutreJoueurJoue(a);
			}

			leJoueurAJoue = true;
			son.setBuffer(ajout);
			son.play();
			initTemps = false;

			tourJoueur1 = !tourJoueur1;
		}
		enfonce = true;
	}
	else
	{
		if (!(Mouse::isButtonPressed(Mouse::Left)) and enfonce)
		{
			enfonce = false;
		}
		if ((tempsLimiteActif) and (tempsLimite - diffTemps <= 0))
		{
			tourJoueur1 = !tourJoueur1;
			leJoueurAJoue = true;
			initTemps = false;
		}
	}
}

int Partie_g::pendreEnMainJeu()
{
	p_fenetre->clear(Color(125, 125, 125));

	dessinerGrille();

	ecrireDetails();

	if (p_fenetre->isOpen())
	{
		if (((Grille_g *)grille)->animerLesPions())
		{
			int victoire = testerVictoire();

			if (victoire != 0)
				return victoire;
			else if (grille->grillePleine())
				return 3;
			else if (leJoueurAJoue)
			{
				prochainTourAvantEvenement();
				leJoueurAJoue = false;
			}
			else
			{
				faireJouerLesJoueurs();

				return -1;
			}
		}
	}
	return -1;
}