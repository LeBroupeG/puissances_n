#include "Bouton.h"

Bouton::Bouton(RenderWindow *fenetre, Vector2f pos, Vector2f size, string nom_, string cheminImage) : RoundedRectangleShape(size , 10, 20)
{
    police.loadFromFile("content/police/Roboto-Regular.ttf");
    nom.setFont(police);
    p_fenetre = fenetre;
    etat = false;
    setOrigin(size.x / 2, size.y / 2);
    setPosition(pos);
    Cnormal = Color(22, 137, 125);
    Cenfonce = Color(20, 227, 205);
    if (cheminImage.compare("NULL")==0)
    {
        avecImage = false;
        setFillColor(Cnormal);
        setOutlineColor(Cnormal);
        setOutlineThickness(3);
        nom.setString(nom_);
        nom.setFillColor(Color::White);
        nom.setCharacterSize(24);
        FloatRect encadrement = nom.getGlobalBounds();
        nom.setOrigin(encadrement.width / 2, 0);
        nom.setPosition(pos.x , pos.y - 15);
    }
    else
    {
        avecImage = true;
        buttonImg.loadFromFile(cheminImage);
        imageButton.loadFromImage(buttonImg);
        setTexture(&imageButton);
        setFillColor(Cnormal);
        setOutlineColor(Cnormal);
    }
}

bool Bouton::rafraichirBouton()
{

    bool changement = enfonce();
    if (etat)
        setFillColor(Cenfonce);
    else
        setFillColor(Cnormal);
    
    return changement;
}

bool Bouton::enfonce()
{
    Vector2f posBouton = getPosition();
    Vector2f position = (Vector2f)Mouse::getPosition(*p_fenetre);
    Vector2f dimention = getSize();
    bool changement = false;
    if ((position.x > (posBouton.x - dimention.x / 2)) and (position.y > (posBouton.y - dimention.y / 2)) and (position.y < (posBouton.y + dimention.y / 2)) and (position.x < (posBouton.x + dimention.x / 2)))
    {
        changement = true;
        if (!etat)
        {
            etat = true;
            activation = clock();
        }
        else
            etat = false;
    }
    return changement;
}

void Bouton::dessiner()
{

    if (!avecImage)
        setTexture(nullptr);

    p_fenetre->draw(*this);

    if (!avecImage)
        p_fenetre->draw(nom);

    if (etat)
        setFillColor(Cenfonce);
    else
        setFillColor(Cnormal);
}