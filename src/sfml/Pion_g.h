#ifndef PION_G_H
#define PION_G_H

#include <SFML/Graphics.hpp>
#include "../core/Pion_c.h"

using namespace sf;
using namespace std;

/**
 * @brief Pion destiné à l'affichage dans une fenêtre graphique
 */
class Pion_g : public Pion_c
{
private:

	

public:
const unsigned int vitesse = 10;
 /**
  * @brief constructeur de la classe Pion_g
  * @param j
  */
	Pion_g(int j);
 /**
  * @brief Cette fonction permet d'animer la chute des pions
  * @return (void)
  */
	void Tomber();

	Vector2i posAct; ///position (en PX) actuelle du pion à l'affichage
	Vector2i posCible; ///position finale du pion après l'animation (en PX
};

#endif