#include "IntArt_c.h"
#include "Partie_ia_c.h"
#include "Partie_c.h"


//DEBUG
#include "../txt/Grille_t.h"

IntArt::IntArt(int niveau_, const Partie_c* partie_, bool premierJoueur)
{
	niveau = niveau_;
	partie = partie_;
	racine = new IANoeud(nullptr, 0);

	racine->tourIA = premierJoueur;
}

IntArt::~IntArt()
{

	if (racine != nullptr)
		racine->libererAvecChaqueEnfant();
}

void IntArt::pionJoue(int c)
{
	IANoeud* tmp = racine;
	racine = racine->getEnfant(c);
	tmp->libererAvecChaqueEnfantSauf(c);
}

void IntArt::lAutreJoueurJoue(int c)
{
	pionJoue(c);
}

int IntArt::jouer()
{

	entrainerMCTS(niveau * ENTRAINEMENT_TOUR);


	//LOG
/*	cout << endl;
	for (int i = 0; i < partie->getGrille().getNbColonne(); i++)
		if (!partie->getGrille().estColonneRemplie(i))
			cout << "node " << i << ": " << racine->getEnfant(i)->getScore() << endl;*/
	//END_LOG

	int rangCoup = racine->choisirCoup();

	pionJoue(rangCoup);

	return rangCoup;
}

void IntArt::initMCTS()
{
	entrainerMCTS(niveau * ENTRAINEMENT_INITIAL);
}

void IntArt::entrainerMCTS(int number)
{
	for (int i = 0; i < number; i++)
	{
		entrainerUneFois();
	}
}

int IntArt::estNoeudTerminal(IANoeud* noeud, Partie_ia_c* partie)
{
	int joueurATester = noeud->tourIA ? 2 : 1;

	if (partie->getGrille().unJoueurAGagne(4, joueurATester))
	{
		if (noeud->tourIA)
			return 2;
		else
			return 1;
	}
	else if (partie->getGrille().unJoueurAGagne(4, 3 - joueurATester))
	{
		if (noeud->tourIA)
			return 1;
		else
			return 2;
	}

	for (int i = 0; i < partie->getGrille().getNbColonne(); i++)
		if (!partie->getGrille().estColonneRemplie(i))
			return 0;

	return 3;
}

void IntArt::entrainerUneFois()
{
	Partie_ia_c* etatPartieActuel = new Partie_ia_c(*partie);
	IANoeud* noeudActuel = racine;
	std::vector<IANoeud*> listeEnfantsNoeud;

	bool plusProfond = true;

	//S�lection: on s�l�ctionne des enfants jusqu'� avoir une node terminale ou une feuille de l'arbre.
	while (plusProfond)
	{


		if (noeudActuel->noeudTerminal != 0 || !noeudActuel->aUnEnfant())
			plusProfond = false;

		else
		{

			for (int i = 0; i < etatPartieActuel->getGrille().getNbColonne(); i++)
				if (!etatPartieActuel->getGrille().estColonneRemplie(i))
					listeEnfantsNoeud.push_back(noeudActuel->getEnfant(i));

			float maxUtc = -1;
			int rangCoup = -1;
			for (IANoeud* noeud : listeEnfantsNoeud)
			{
				if (noeud->getUtc() > maxUtc)
				{
					maxUtc = noeud->getUtc();
					rangCoup = noeud->getCoup();
				}
			}
			etatPartieActuel->unJoueurJoue(rangCoup, noeudActuel->tourIA);

			etatPartieActuel->prochainTourAvantEvenement();

			noeudActuel = noeudActuel->getEnfant(rangCoup);

			listeEnfantsNoeud.clear();

		}
	}

	//Expension, pas d'expension s'il s'agit d'une node terminale: on cr��e un enfant au noeud feuille s�lectionn�.
	IANoeud* noeudExpension = noeudActuel;
	noeudExpension->noeudTerminal = estNoeudTerminal(noeudExpension, etatPartieActuel);

	if (noeudActuel->noeudTerminal == 0)
	{
		for (int i = 0; i < etatPartieActuel->getGrille().getNbColonne(); i++)
			if (!etatPartieActuel->getGrille().estColonneRemplie(i))
				listeEnfantsNoeud.push_back(noeudExpension->getEnfant(i));

		int coupExpension = listeEnfantsNoeud[rand() % listeEnfantsNoeud.size()]->getCoup();

		etatPartieActuel->unJoueurJoue(coupExpension, noeudExpension->tourIA);
		etatPartieActuel->prochainTourAvantEvenement();

		noeudExpension = noeudActuel->getEnfant(coupExpension);


		//Simulation: on joue des coups al�atoires jusqu'� tomber sur une node terminale.
		noeudActuel = noeudExpension;
		noeudActuel->noeudTerminal = estNoeudTerminal(noeudActuel, etatPartieActuel);

		while (noeudActuel->noeudTerminal == 0)
		{
			listeEnfantsNoeud.clear();

			for (int i = 0; i < etatPartieActuel->getGrille().getNbColonne(); i++)
				if (!etatPartieActuel->getGrille().estColonneRemplie(i))
					listeEnfantsNoeud.push_back(noeudActuel->getEnfant(i));


			int coup = listeEnfantsNoeud[rand() % listeEnfantsNoeud.size()]->getCoup();

			etatPartieActuel->unJoueurJoue(coup, noeudActuel->tourIA);
			etatPartieActuel->prochainTourAvantEvenement();

			noeudActuel = noeudActuel->getEnfant(coup);

			noeudActuel->noeudTerminal = estNoeudTerminal(noeudActuel, etatPartieActuel);
		}
	}

	delete etatPartieActuel;
	etatPartieActuel = nullptr;


	//R�tropropagation: on ajoute un test et potentiellement une victoire � tous les ascendants de la node cr��e lors de l'expension.

	int victoireId = noeudActuel->noeudTerminal;

	noeudExpension->tests++;
	if ((victoireId == 1 && !noeudExpension->tourIA) || (victoireId == 2 && noeudExpension->tourIA))
		noeudExpension->victoires++;


	while (noeudExpension->getParent() != nullptr)
	{
		noeudExpension = noeudExpension->getParent();

		noeudExpension->tests++;

		if ((victoireId == 1 && !noeudExpension->tourIA) || (victoireId == 2 && noeudExpension->tourIA))
			noeudExpension->victoires++;
	}

}