#include "Partie_c.h"
#include <ctime>
#include <cstdlib>



Partie_c::Partie_c()
{
}

Partie_c::Partie_c(int gameMode_, unsigned int largeurGrille, unsigned int hauteurGrille, bool isTimeLimited, unsigned int timeLimit_, bool iAActivated_, unsigned int IALevel)
{
	srand(time(0));
	 tourJoueur1 = rand() % 2;

	//cout << "INIT, tour: " << (turnToPlayer1 ? 1 : 2) << endl;
	
	tourAvantEvenement = 3;
	modeDeJeu = gameMode_;

	if (!(largeurGrille <= 4 or hauteurGrille <= 4))
	{
		if (isTimeLimited)
		{
			tempsLimiteActif = isTimeLimited;
			tempsLimite = timeLimit_;
		}

		iAActive = iAActivated_;
		if (iAActivated_)
		{
			intArt = new IntArt(IALevel, this, tourJoueur1);
		}
		if (gameMode_ == 4)
		{
			unsigned int h, t;
			h = hauteurGrille;
			t = largeurGrille;
			if (t < h)
			{
				if (h % 2 == 0)
				{
					t = h + 1;
					h++;
				}
				else
					t = h;
			}
			else
			{
				if (t % 2 == 0)
					t++;
				h = t;
			}
		}
	}



	return ;
}

Partie_c::Partie_c(const Partie_c &partie)
{
	modeDeJeu = partie.modeDeJeu;
	grille = new Grille_c(*partie.grille);
	tempsLimiteActif = partie.tempsLimiteActif;
	tempsLimite = partie.tempsLimite;
	insererPionsParDessus = partie.insererPionsParDessus;
	iAActive = partie.iAActive;
	tourAvantEvenement = partie.tourAvantEvenement;
}

Partie_c::~Partie_c()
{
	if (grille != nullptr)
		delete grille;
	if (intArt != nullptr)
		delete intArt;
}

void Partie_c::prochainTourAvantEvenement()
{
	if (modeDeJeu == 0)
		return;
	else if (modeDeJeu <= 2)
	{
		declencherEvenement();
	}
	else
	{
		if (tourAvantEvenement == 0)
		{
			declencherEvenement();
			tourAvantEvenement = 3;
		}else{
			tourAvantEvenement--;
		}
	}
}

void Partie_c::declencherEvenement()
{
	switch (modeDeJeu)
	{
	case 1:
		grille->decalage(0, -1, 1, true);
		grille->decalage(0, -1);
		break;
	case 2:
		grille->decalage(1, 0, 1, true);
		break;
	case 3:
		insererPionsParDessus = !insererPionsParDessus;
		break;
	case 4:
		grille->rotaterAvecGravite(90);
		break;
	case 5:
		grille->rotaterSansGravite(90);
		break;
	}
}

int Partie_c::pendreEnMainJeu()
{
	dessinerGrille();

	if (iAActive)
		intArt->initMCTS();

	while (true)
	{
		
		faireJouerLesJoueurs();

		if (leJoueurAJoue)
		{
			prochainTourAvantEvenement();

			nettoyerEcran();

			ecrireDetails();

			leJoueurAJoue = false;
		}

		dessinerGrille();

		int victoire = testerVictoire();
		if (victoire != 0)
			return victoire;
		else if(grille->grillePleine())
			return 3;
		if(grille->grillePleine()) return -1;
	}
}

bool Partie_c::tomberAuMilieu()
{
	return modeDeJeu == 3;
}

const Grille_c &  Partie_c::getGrille() const
{
	return *grille;
}