#include "IANoeud.h"

#include <math.h>
#include <iostream>

IANoeud::IANoeud()
{

}

IANoeud::IANoeud(IANoeud* parent_, int coup_)
{
	parent = parent_;
	coup = coup_;
	if (parent != nullptr)
		tourIA = !(parent->tourIA);
	else
		tourIA = true;
}

IANoeud::~IANoeud()
{
}

bool IANoeud::aUnEnfant()
{
	for (std::pair<int, IANoeud*> node : enfants)
		if (node.second->getUtc() < INFINITY)
			return true;

	return false;
}

float IANoeud::getUtc()
{
	if (tests == 0)
		return INFINITY;

	return (victoires / tests) + C * (float)sqrt(log(parent->tests) / tests);
}

IANoeud* IANoeud::getEnfant(int moove)
{
	if (enfants.count(moove) == 0)
	{
		IANoeud* tmp = new IANoeud(this, moove);
		enfants.emplace(moove, tmp);
	}
	
	return enfants[moove];
}

IANoeud* IANoeud::getParent()
{
	return parent;
}

int IANoeud::getCoup()
{
	return coup;
}


int IANoeud::choisirCoup()
{
	float max = 0;
	int coup = -1;
	for (std::pair<int, IANoeud*> node : enfants)
	{
		if (node.second->noeudTerminal == 1)
			return node.first;

		else if (node.second->noeudTerminal != 2 and node.second->getScore() >= max)
		{
			max = node.second->getScore();
			coup = node.first;
		}
	}
	return coup;
}

float IANoeud::getScore()
{
	if (tests == 0)
		return 0;
	else
	{
		return (float)victoires / (float)tests;
	}
}

void IANoeud::libererAvecChaqueEnfant()
{
	for (std::pair<int, IANoeud*> node : enfants)
	{
	    node.second->libererAvecChaqueEnfant();
	}
	delete this;
}

void IANoeud::libererAvecChaqueEnfantSauf(int coup)
{
	for (std::pair<int, IANoeud*> node : enfants)
	{
		if (node.first != coup)
			node.second->libererAvecChaqueEnfant();
		else
			node.second->parent = nullptr;
	}
	delete this;
}
