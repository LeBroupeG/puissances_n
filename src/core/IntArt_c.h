#ifndef INTART_H
#define INTART_H

#include "IANoeud.h"
#include <vector>

class Partie_ia_c;
class Partie_c;

/**
* @brief Classe de L'IA. Elle pourra prendre le réle du joueur adverse lors des parties de puissance_n.
**/
class IntArt
{
private:

    const int ENTRAINEMENT_INITIAL = 100, ENTRAINEMENT_TOUR = 80; ///Constantes permettant de moduler la force de l'IA en changeant la puissance de calcul qui lui est allouée.

    int niveau; ///Variable permettant de moduler le niveau de l'IA.

    const Partie_c* partie; /// Partie donnée à l'IA pour pouvoir faire des simulations internes.

    IANoeud * racine; /// Racine de l'arbre permettant à l'IA d'explorer les possibilités du jeu.

    /**
     * @brief Entraine l'IA un certain nombre de fois, indiqué en paramétre.
     * @param nombre
     * @return (void)
     */
    void entrainerMCTS(int nombre);

    
    /**
     * @brief Entraine l'IA une fois, passant par 4 étapes: sélection, expension, simulation, rétropropagation. A la fin de cet entrainement, l'arbre gagne un noeud et tous les ascendants de ce noeud voient leur valeurs gagner en précision.
     * @return (void)
     */
    void entrainerUneFois();

    /**
     * @brief   Met à joue la valeur winnerNode de l'IANode donnée en paramètre.
     * @param noeud
     * @param partie
     * @return int
     */
    int estNoeudTerminal(IANoeud* noeud, Partie_ia_c* partie);


    /**
     * @brief Décale la racine de l'arbre vers un fils de la racine actuelle, suivant le coup joué.
     * @param c
     * @return (void)
     */
    void pionJoue(int c);

public:

    /**
     * @brief A appeller au début de la partie, avant le premier coup.
     * @return (void)
     */
    void initMCTS();

    /**
     * @brief A appeller au moment ou l'autre joueur joue, pour mettre l'IA au courant du coup joué.
     * @param c
     * @return (void)
     */
    void lAutreJoueurJoue(int c);

    /**
     * @brief Renvoie le coup que l'IA décide de jouer.
     * @return int
     */
    int jouer();

    /**
     * @brief Constructeur de la classe permettant de créer un IA avec les paramètres voulues
     * @param niveau_
     * @param partie_
     * @param premierJoueur
     */
    IntArt(int niveau_  , const Partie_c* partie_, bool premierJoueur);
    /**
     * @brief destructeur
     */
    ~IntArt();
    
};



#endif