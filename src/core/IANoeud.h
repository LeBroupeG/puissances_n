#ifndef IANODE_C_H
#define IANODE_C_H

#include <map>

/**
* @brief Classe représentant un noeud de l'arbre qui sera utilisé par l'intelligence artificiel.
* Chaque noeud correspond à un état du jeu.
**/
class IANoeud
{

private:
	IANoeud *parent; ///Pointeur sur le noeud parent de ce noeud dans l'arbre.

	std::map<int, IANoeud *> enfants; ///Pointeurs sur les noeuds enfants de ce noeud dans l'arbre.

	const float C = 1.8f; ///Constante servant à définir la tendance de l'arbre à s'expendre en largeur, déterminée empiriquement.

	int coup; ///Valeur correspondant à la colone dans laquel un pion à été posé depuis l'état du jeu du noeud parent pour atteindre ce noeud.

public:
	bool tourIA; /// Permet de savoir si ce noeud correspond à un état du jeu ou l'IA est en train de jouer.

	int tests = 0; /// Nombre de fois que ce noeud ou un descendant de ce noeud à été testé.

	int victoires = 0; /// Nombre de fois que ce noeud et ses descendants on mené à une victoire pour le joueur représenté par ce noeud.

	int noeudTerminal = 0; /// Permet de savoir si cette node est terminale. 0: non, 1: Victoire de l'IA, 2: Victoire du joueur, 3: match null;

	/**
  * @brief Constructeur par défaut
  */
	IANoeud();
	/**
  * @brief Constructeur à paramètres
  * @param parent_
  * @param coup_
  */
	IANoeud(IANoeud *parent_, int coup_);
	/**
  * @brief Destructeur
  */
	~IANoeud();

	/**
  * @brief permet de récupérer les enfants de la branche de "coup"
  * @param coup
  * @return 
  */
	IANoeud *getEnfant(int coup);
	/**
  * @brief permet de récupérer les parents de la branche de "coup"
  * @return 
  */
	IANoeud *getParent();
	int getCoup();

	/**
  * @brief Renvoie la valeur en Utc de cette node, cette valeur permet de faire des choix lors de l'étape de sélection des MCTS. Elle est calculée grace au nombre de tests, de victoires et de tests du parent.
  * @return float
  */
	float getUtc();

	/**
  * @brief Renvoie faux si les enfants de ce noeud n'ont pas encore été instanciés, permet de terminer l'étape de sélection.
  * @return bool
  */
	bool aUnEnfant();

	/**
  * @brief Renvoie le "moove" correspondant à la node enfant avec le plus gros score. Utilisé sur la racine de l'arbre pour savoir quel coup jouer aprés l'entrainement.
  * @return int
  */
	int choisirCoup();

	/**
	* @brief Renvoie le score de la node: 0 si la node n'a pas été testée (improbable voir impossible), wins/tests sinon.
	* @return (void)
	**/
	float getScore();

	/**
	* @brief supprime le noeud et tous ses descendants, libére totalement la mémoire de l'IA quand utilisé sur la racine de l'arbre.
	* @return (void)
	**/
	void libererAvecChaqueEnfant();

	/**
  * @brief  supprime le noeud et tous ses enfants sauf celui indiqué, supprime aussi tous les descendants des enfants supprimés. Permet de décaler la racine de l'arbre sans fuite de mémoire.
  * @param moove
  * @return (void)
  */
	void libererAvecChaqueEnfantSauf(int moove);
};

#endif