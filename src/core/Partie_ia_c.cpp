#include "Partie_ia_c.h"

 Partie_ia_c::Partie_ia_c(const Partie_c& partie) : Partie_c(partie)
{
}

bool Partie_ia_c::unJoueurJoue(int c, bool p1)
{
	int p = p1 ? 1 : 2;

	return grille->ajouterPion(p, c, tomberAuMilieu(), insererPionsParDessus);
}

void Partie_ia_c::dessinerGrille()
{

}
void Partie_ia_c::nettoyerEcran()
{

}
void Partie_ia_c::ecrireDetails()
{

}
int Partie_ia_c::testerVictoire()
{
	return 0;
}
void Partie_ia_c::faireJouerLesJoueurs()
{

}