#include "Pion_c.h"

Pion_c::Pion_c(){}

Pion_c::Pion_c(const Pion_c& pion_)
{
    posX = pion_.posX;
    posY = pion_.posY;
    idJoueur = pion_.idJoueur;
}

Pion_c::Pion_c(int idJoueur_){
    idJoueur = idJoueur_;
}

void Pion_c::setPos(int posX_, int posY_){
    posX = posX_;
    posY = posY_;
}