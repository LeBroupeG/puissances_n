#include "Grille_c.h"
#include <iostream>
#include <assert.h>
#include <math.h>

using namespace std;


Grille_c::Grille_c(const Grille_c& grille)
{
    nbLigne = grille.nbLigne;
    nbColonne = grille.nbColonne;

    int maxi = max(nbLigne, nbColonne);
    maxi++;

    pions = new Pion_c * *[maxi];
    pions_buff = new Pion_c * *[maxi];

    for (int y = 0; y < maxi; y++)
    {
        pions[y] = new Pion_c * [maxi];
        pions_buff[y] = new Pion_c * [maxi];
    }

    for (int y = 0; y < maxi; y++)
    {
        for (int x = 0; x < maxi; x++)
        {
            pions[x][y] = nullptr;
        }
    }

    for (int y = 0; y <= nbLigne; y++)
    {
        for (int x = 0; x <= nbColonne; x++)
        {
            if (grille.pions[x][y] != nullptr)
                pions[x][y] = new Pion_c(*grille.pions[x][y]);

        }
    }
}


Grille_c::Grille_c(int largeur, int hauteur)
{
    init(largeur, hauteur);
}

Grille_c::~Grille_c()
{
    int maxi = max(nbLigne, nbColonne);
    maxi++;
    for (int x = 0; x < maxi; x++)
    {
        for (int y = 0; y < maxi; y++)
        {
            if (getPion(x, y) != nullptr)
            {
                delete pions[x][y];
            }
        }
        delete[] pions[x];
        delete[] pions_buff[x];
    }
    delete[] pions;
    delete[] pions_buff;
}

void Grille_c::init(int largeur, int hauteur)
{
    nbLigne = hauteur;
    nbColonne = largeur;

    int maxi = max(nbLigne, nbColonne);
    maxi++;

    pions = new Pion_c * *[maxi];
    pions_buff = new Pion_c * *[maxi];

    for (int y = 0; y < maxi; y++)
    {
        pions[y] = new Pion_c * [maxi];
        pions_buff[y] = new Pion_c * [maxi];
    }

    for (int y = 0; y < maxi; y++)
    {
        for (int x = 0; x < maxi; x++)
        {
            pions[x][y] = nullptr;
        }
    }
}

bool Grille_c::estColonneRemplie(int colonne) const {
    assert(colonne < this->nbColonne);
    int curseurY = 0;
    while (curseurY < nbLigne) {
        if (getPion(colonne, curseurY) == nullptr) return false;
        curseurY++;
    }
    return true;
}

bool Grille_c::insererPion(int colonne, Pion_c* pion, bool tomberMilieu, bool insererParDessus)
{
    bool posYTrouvee = false;
    int line;
    if (colonne >= nbColonne) return false;
    if (insererParDessus)
    {
        line = 0;
        if (tomberMilieu)
            line = nbLigne / 2;

        int posY = nbLigne - 1;

        while (!posYTrouvee)
        {
            Pion_c* buff = getPion(colonne, posY);

            if ((buff != nullptr && posY == nbLigne - 1) || posY < line - 1)
            {
                delete pion;
                return false;
            }
            else if (buff == nullptr && posY != line - 1)
            {
                posY--;
            }
            else
            {
                posYTrouvee = true;
            }
        }
        pion->setPos(colonne, posY + 1);
        pions[colonne][posY + 1] = pion;
        return true;
    }
    else
    {
        line = nbLigne;
        if (tomberMilieu)
            line = nbLigne / 2 + 1;

        int posY = 0;

        while (!posYTrouvee)
        {
            Pion_c* buff = getPion(colonne, posY);

            if ((buff != nullptr && posY == 0) || posY > line) // Teste l'impossibilité d'insérer
            {
                delete pion;
                return false;
            }

            else if (buff == nullptr && posY < line) // Test s'il est possible de poursuivre la recherche
            {
                posY++;
            }
            else //Teste si
            {
                posYTrouvee = true;
            }
        }
        pion->setPos(colonne, posY - 1);
        pions[colonne][posY - 1] = pion;
        return true;
    }
}

Pion_c* Grille_c::getPion(int x, int y) const
{
    if (x <= nbColonne && y <= nbLigne && x >= 0 && y >= 0)
        return pions[x][y];
    else
        return nullptr;
}

int Grille_c::getNbColonne() const
{
    return nbColonne;
}

int Grille_c::obtenirNbLigne() const{
    return nbLigne;
}

bool Grille_c::deplacerPion(int x, int y, Pion_c* pion)
{
    if (pion != nullptr)
    {
        if (pions[x][y] == nullptr)
        {
            pions[x][y] = pion;
            pions[pion->posX][pion->posY] = nullptr;
            pion->setPos(x, y);
        }
        else
        {
            Pion_c* buff = pions[x][y];

            pions[x][y] = pion;
            pions[pion->posX][pion->posY] = buff;

            pion->setPos(x, y);
        }

        return true;
    }
    return false;
}

void Grille_c::deplacerPionDepuisJusquA(Pion_c*** depuis, Pion_c*** jusquA, int xOrigine, int yOrigine, int nouveauX, int nouveauY)
{
    if (depuis[xOrigine][yOrigine] != nullptr)
    {
        assert(jusquA[nouveauX][nouveauY] == nullptr);
        jusquA[nouveauX][nouveauY] = depuis[xOrigine][yOrigine];
        jusquA[nouveauX][nouveauY]->setPos(nouveauX, nouveauY);
    }
}

int Grille_c::rechercheVectorielle(int x, int y, Pion_c* pionDepart) const
{
    Pion_c* buff = pionDepart;
    int curseur = 0;
    int cur_largeur = 0;
    while (buff->idJoueur == pionDepart->idJoueur && pionDepart->posX + x * curseur < this->nbColonne && pionDepart->posX + x * curseur >= 0 && pionDepart->posY + y * curseur < this->nbLigne && pionDepart->posY + y * curseur >= 0)
    {
        cur_largeur++;
        curseur++;
        buff = getPion(pionDepart->posX + x * curseur, pionDepart->posY + y * curseur);
        if (buff == nullptr)
            return cur_largeur;
    }
    return cur_largeur;
}

bool Grille_c::unJoueurAGagne(int longueur, int id) const
{

    int maxi = max(nbLigne, nbColonne);

    for (int x = 0; x < maxi; x++)
    {
        for (int y = 0; y < maxi; y++)
        {

            Pion_c* pionAct = getPion(x, y);

            if (pionAct != nullptr)
            {
                if (pionAct->idJoueur == id)
                {
                    for (int x = -1; x <= 1; x++)
                    {
                        for (int y = -1; y <= 1; y++)
                        {

                            if (y == 0 && x == 0)
                                break;
                            int longAct = 0;
                            //Détermination de la longueur de la chaîne sur la ligne (Droite)
                            longAct += rechercheVectorielle(x, y, pionAct) + rechercheVectorielle(-x, -y, pionAct) - 1;
                            //Teste si la longueur de la chaîne est suffisante
                            if (longAct >= longueur)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }

    return false;
}

void Grille_c::decalageVectoriel(Pion_c* pion, int vectX, int vectY, bool boucle)
{
    if (pion != nullptr)
    {
        assert(vectX == 0 || vectY == 0);
        Pion_c* buff = nullptr;
        buff = getPion(pion->posX + vectX, pion->posY + vectY);

        if (vectX != 0 && buff == nullptr)
        {
            if (pion->posX + vectX >= 0 && pion->posX + vectX < nbColonne)
                deplacerPion(pion->posX + vectX, pion->posY, pion);

            else if ((pion->posX + vectX == -1 || pion->posX + vectX == nbColonne) && boucle)
                deplacerPion(nbColonne, pion->posY, pion);

            else if (pion->posX + vectX == nbColonne + 1)
                deplacerPion(0, pion->posY, pion);
        }
        else if (vectY != 0 && buff == nullptr)
        {
            if (pion->posY + vectY >= 0 && pion->posY + vectY < nbLigne)
            {
                deplacerPion(pion->posX, pion->posY + vectY, pion);
            }

            else if ((pion->posY + vectY == -1 || pion->posY + vectY == nbLigne) && boucle)
                deplacerPion(pion->posX, nbLigne, pion);

            else if (pion->posY + vectY == nbLigne + 1)
                deplacerPion(pion->posX, 0, pion);
        }
    }
}

void Grille_c::decalage(int vectX, int vectY, int lim, bool boucle)
{
    assert(vectY == 0 || vectX == 0);

    if (lim == -1)
        lim = max<int>(nbColonne, nbLigne);
    for (int i = 0; i < lim; i++)
    {
        if (vectX == 0)
            decalageVertical(vectY, boucle);
        else
            decalageHorizontal(vectX, boucle);
    }
}

void Grille_c::decalageVertical(int direction, bool boucle)
{
    for (int y = 0; y < nbLigne; y++)
    {
        for (int x = 0; x < nbColonne; x++)
        {

            switch (direction)
            {
            case -1:
                decalageVectoriel(getPion(x, y), 0, direction, boucle);
                break;
            case 1:
                decalageVectoriel(getPion(x, nbLigne - 1 - y), 0, direction, boucle);
                break;
            }
        }
    }
    for (int x = 0; x < nbColonne; x++)
    {
        decalageVectoriel(getPion(x, nbLigne), 0, direction);
    }
}

void Grille_c::decalageHorizontal(int direction, bool boucle)
{
    for (int x = 0; x < nbColonne; x++)
    {
        for (int y = 0; y < nbLigne; y++)
        {
            switch (direction)
            {
            case -1:
                decalageVectoriel(getPion(x, y), direction, 0, boucle);
                break;
            case 1:
                decalageVectoriel(getPion(nbColonne - 1 - x, y), direction, 0, boucle);
                break;
            }
        }
    }

    for (int y = 0; y < nbLigne; y++)
    {
        decalageVectoriel(getPion(nbColonne, y), direction, 0);
    }
}

void Grille_c::rotaterAvecGravite(int angle)
{
    assert(angle == 90 || angle == 180 || angle == -90);

    int maxi = max(nbLigne, nbColonne);
    for (int x = 0; x < maxi; x++)
    {
        for (int y = 0; y < maxi; y++)
        {
            pions_buff[x][y] = nullptr;
        }
    }

    for (int x = 0; x < nbColonne; x++)
    {
        for (int y = 0; y < nbLigne; y++)
        {
            switch (angle)
            {
            case 90:
                deplacerPionDepuisJusquA(pions, pions_buff, x, y, nbLigne - 1 - y, x);
                break;
            case -90:
                deplacerPionDepuisJusquA(pions, pions_buff, x, y, y, nbColonne - 1 - x);
                break;
            case 180:
                deplacerPionDepuisJusquA(pions, pions_buff, x, y, nbColonne - 1 - x, nbLigne - 1 - y);
                break;
            }
        }
    }

    for (int x = 0; x < maxi; x++)
    {
        for (int y = 0; y < maxi; y++)
        {
            pions[x][y] = nullptr;
        }
    }

    for (int x = 0; x < maxi; x++)
    {
        for (int y = 0; y < maxi; y++)
        {
            deplacerPionDepuisJusquA(pions_buff, pions, x, y, x, y);
        }
    }

    int nbLigneOrigine = nbLigne;
    switch (angle)
    {
    case 90:

        nbLigne = nbColonne;
        nbColonne = nbLigneOrigine;

        break;
    case -90:

        nbLigne = nbColonne;
        nbColonne = nbLigneOrigine;
        break;
    }
    decalage(0,-1);
}

void Grille_c::rotaterSansGravite(int angle)
{
    assert(angle == 90 || angle == 180 || angle == -90);
    int maxi = max(nbLigne, nbColonne);
    for (int x = 0; x < maxi; x++)
    {
        for (int y = 0; y < maxi; y++)
        {
            pions_buff[x][y] = nullptr;
        }
    }

    for (int x = 0; x < nbColonne; x++)
    {
        for (int y = 0; y < nbLigne; y++)
        {
            switch (angle)
            {
            case 90:
                deplacerPionDepuisJusquA(pions, pions_buff, x, y, nbLigne - 1 - y, x);
                break;
            case -90:
                deplacerPionDepuisJusquA(pions, pions_buff, x, y, y, nbColonne - 1 - x);
                break;
            case 180:
                deplacerPionDepuisJusquA(pions, pions_buff, x, y, nbColonne - 1 - x, nbLigne - 1 - y);
                break;
            }
        }
    }

    for (int x = 0; x < maxi; x++)
    {
        for (int y = 0; y < maxi; y++)
        {
            pions[x][y] = nullptr;
        }
    }

    for (int x = 0; x < maxi; x++)
    {
        for (int y = 0; y < maxi; y++)
        {
            deplacerPionDepuisJusquA(pions_buff, pions, x, y, x, y);
        }
    }

    int nombreLigneOrigine = nbLigne;
    switch (angle)
    {
    case 90:

        nbLigne = nbColonne;
        nbColonne = nombreLigneOrigine;

        break;
    case -90:

        nbLigne = nbColonne;
        nbColonne = nombreLigneOrigine;
        break;
    }
}

void Grille_c::dessinerGrille() {}

bool Grille_c::ajouterPion(const int joueur, unsigned int colonne, bool milieu, bool insererParDessus) 
{
    Pion_c* temp = new Pion_c(joueur);
    return insererPion(colonne, temp, milieu, insererParDessus);
}

int Grille_c::posPoint() { return 0; }

bool Grille_c::grillePleine(){
    int nbColonnePleine = 0;
    for (int i = 0; i < nbColonne; i++)
    {
        if(estColonneRemplie(i))
            nbColonnePleine++;
    }
    return (nbColonnePleine >= nbColonne);
    
}