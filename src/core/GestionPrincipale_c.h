#ifndef GESTION_PRINCIPALE_C
#define GESTION_PRINCIPALE_C

#include "Partie_c.h"
#include <stdlib.h>

using namespace std;

class GestionPrincipale_c
{
protected:
    Partie_c * partiePrincipale; /// La partie principale
    bool estChainee, modeDeJeuAleaChainage, tailleGrilleAleaChainage, tempsLimitAleaChainage; /// paramètres de partie
    unsigned int nbChainage, rgChainage = 0; /// Paramètres de chaînage de partie
    int score1 = 0, score2 = 0, scoreIA = 0; /// Scores joueur + IA

    bool iAActivee; /// Ia est active ?

public:

    GestionPrincipale_c();
    virtual ~GestionPrincipale_c();

    /**
     * @brief Retourne un pointeur vers la partie principale
     * 
     * @return Partie_c* 
     */
    Partie_c * getPartie();

    /**
     * @brief Gère les parties
     * 
     */
    virtual void prendreEnMain();

    /**
     * @brief Initialise les paramètres généraux du jeu
     * 
     */
    virtual void init() = 0;

    /**
     * @brief Créer une nouvelle partie
     * 
     */
    virtual void creerSession() = 0;

    /**
     * @brief Supprime la partie en cours
     * 
     */
    void supprimerSession();

    /**
     * @brief Efface l'écran
     * 
     */
    virtual void nettoyerEcran() = 0;

    virtual void afficherEcranAccueil() = 0;
    virtual void afficherEcranFin() = 0;
    virtual void ecrireScore() = 0;
};

#endif