#include "GestionPrincipale_c.h"
using namespace std;

GestionPrincipale_c::GestionPrincipale_c()
{
    partiePrincipale = nullptr;
    srand(time(NULL));
}

GestionPrincipale_c::~GestionPrincipale_c()
{
    if (partiePrincipale != nullptr)
    {
        delete partiePrincipale;
        partiePrincipale = nullptr;
    }
}

void GestionPrincipale_c::supprimerSession()
{
    delete partiePrincipale;
}

void GestionPrincipale_c::prendreEnMain()
{
    afficherEcranAccueil();
    init();
    while (rgChainage < nbChainage)
    {
        nettoyerEcran();
        ecrireScore();
        creerSession();
        int gagnant = partiePrincipale->pendreEnMainJeu();
        if (gagnant == 1)
            score1++;
        else if (gagnant == 2)
            score2++;
        rgChainage++;
    }
    afficherEcranFin();
}

Partie_c *GestionPrincipale_c::getPartie()
{
    return partiePrincipale;
}