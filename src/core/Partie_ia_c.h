#ifndef PARTIE_IA_C_H
#define PARTIE_IA_C_H

#include "Partie_c.h"

using namespace std;

/**
 * @brief Cette classe est la classe partie_c redéfinie pour permettre une utilisation par l'IA. Pour plus de détails sur les fonctions, voir la documentation de Partie_c
 */
class Partie_ia_c : public Partie_c
{
public:
    /**
     * @brief COnstructeur par copie d'une Partie_c
     * @param partie
     */
    Partie_ia_c(const Partie_c& partie);

    void dessinerGrille();
    void nettoyerEcran();
    void ecrireDetails();
    int testerVictoire();
    void faireJouerLesJoueurs();
    /**
     * @brief Permet de faire savoir à l'IA qu'un joueur joue
     * @param c
     * @param j1
     * @return bool
     */
    bool unJoueurJoue(int c, bool j1);
};

#endif