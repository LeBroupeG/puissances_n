#ifndef GRILLE_C_H
#define GRILLE_C_H

#include "Pion_c.h"

class Grille_c{
    
    protected:
    Pion_c*** pions, ***pions_buff;
    int nbLigne = 5;
    int nbColonne = 5;

public:

    /**
    *   @brief Constructeur Permettant de construire une grille de hauteur et de largeur donnée
    *   @param width : Largeur
    *   @param height : Hauteur
    **/
    Grille_c();

    /**
     * @brief Constructeur par copie
     * 
     */
    Grille_c(const Grille_c(&grille));

    /**
     * @brief Construit une grille de largeur * hauteur
     * 
     * @param largeur 
     * @param hauteur 
     */
    Grille_c(int largeur, int hauteur);
    virtual ~Grille_c();

    /**
    *   @brief Test de régression, Utilise des asserts si une erreur est détectée
    **/
    void testRegression();

    /**
     * @brief Déplace le pion "pion" vers (x,y)
     * 
     * @param x 
     * @param y 
     * @param pion 
     * @return true 
     * @return false 
     */
    bool deplacerPion(int x, int y, Pion_c* pion);

    /**
     * @brief Déplace le pion de coordonnées (xOrigine, yOrigine) de la grille d'origine vers la seconde grille aux coordonnées (nouveauX, nouveauY)
     * 
     * @param depuis 
     * @param jusquA 
     * @param xOrigine 
     * @param yOrigine 
     * @param nouveauX 
     * @param nouveauY 
     */
    void deplacerPionDepuisJusquA(Pion_c*** depuis, Pion_c*** jusquA, int xOrigine, int yOrigine, int nouveauX, int nouveauY);

    /**
     * @brief Initialise une grille
     * 
     * @param largeur 
     * @param hauteur 
     */
    void init(int largeur, int hauteur);

    /**
     * @brief Insère un pion dans la colonne "colonne"
     * 
     * @param colonne 
     * @param pion 
     * @return true 
     * @return false 
     */
    bool insererPion(int colonne, Pion_c* pion, bool = false, bool = true);

    /**
     * @brief ajoute Un pion dans la grille avec des paramètres spéciaux
     * 
     * @param joueur 
     * @param colonne 
     * @param tombeMilieu 
     * @param insererParDessus 
     * @return true 
     * @return false 
     */
    virtual bool ajouterPion(const int joueur, unsigned int colonne, bool tombeMilieu = false, bool insererParDessus = true);

/**
 * @brief Retourne l'état de la colonne (Remplie ou non)
 * 
 * @param colonne 
 * @return true 
 * @return false 
 */
    bool estColonneRemplie(int colonne) const;

/**
 * @brief Retourne un pointeur vers un pion de coordonnées x,y
 * 
 * @param x 
 * @param y 
 * @return Pion_c* 
 */
    Pion_c* getPion(int x, int y) const;

    /**
     * @brief Retourne le nombre de colonnes
     * 
     * @return int 
     */
    int getNbColonne() const;

    /**
     * @brief Retourne le nombre de ligne
     * 
     * @return int 
     */
    int obtenirNbLigne() const;

    /**
     * @brief Vérifie si un joueur a gagné
     * 
     * @param longueur 
     * @param id 
     * @return true 
     * @return false 
     */
    bool unJoueurAGagne(int longueur, int id) const;

    /**
     * @brief Recherche combien de pions sont alignés selon le vecteur (x,y) à partir du pion "pionDepart"
     * 
     * @param x 
     * @param y 
     * @param pionDepart 
     * @return int 
     */
    int rechercheVectorielle(int x, int y, Pion_c* pionDepart) const;

    /**
     * @brief Décale le pion "pion" selon le vecteur vectX, VectY
     * 
     * @param pion 
     * @param vectX 
     * @param vectY 
     * @param boucle 
     */
    void decalageVectoriel(Pion_c* pion, int vectX, int vectY, bool boucle = false);



    ///////////////////////// Opération sur la grille //////////////////

    /**
     * @brief Décale les pions
     * 
     * @param vectX 
     * @param vectY 
     * @param lim 
     * @param boucle 
     */

    void decalage(int vectX, int vectY, int lim = -1, bool boucle = false);

    /**
     * @brief décale les pions horizontalement
     * 
     * @param boucle 
     */
    virtual void decalageHorizontal(int, bool boucle = false); /// Mode de jeu "Shift Horizontal"

    /**
     * @brief Décale les pions verticalement
     * 
     * @param boucle 
     */
    virtual void decalageVertical(int, bool boucle = false); /// Mode de jeu "Shift Vertical"

    /**
     * @brief Tourne la grille d'un angle de "angle" (90, 180, 270) avec gravité
     * 
     * @param angle 
     */
    virtual void rotaterAvecGravite(int angle); /// Mode de jeu "Rotation classique"

    /**
     * @brief Tourne la grille d'un angle de "angle" (90, 180, 270) sans gravité
     * 
     * @param angle 
     */
    virtual void rotaterSansGravite(int angle); /// Mode de jeu "Rotation par centre de gravité"

    /**
     * @brief 
     * 
     * @return int 
     */
    virtual int posPoint();

    /**
     * @brief Déssine la grille
     * 
     */
    virtual void dessinerGrille();

    /**
     * @brief renvoie vrai si la grille est pleine
     * @return bool
     */
    bool grillePleine();
};

#endif