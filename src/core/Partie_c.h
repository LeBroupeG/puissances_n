#ifndef PARTIE_C_H
#define PARTIE_C_H

#include "Grille_c.h"
#include "IntArt_c.h"
#include <ctime>
#include <thread>
#include <iostream>

using namespace std;

class Partie_c
{
protected:
    static const int STANDARD_SMALL_X = 7, STANDARD_SMALL_Y = 6, STANDARD_MIDDLE_X = 8, STANDARD_MIDDLE_Y = 7, STANDARD_LARGE_X = 10, STANDARD_LARGE_Y = 8;
    static const int CHANGING_GRAV_SMALL_X = 6, CHANGING_GRAV_SMALL_Y = 7, CHANGING_GRAV_MIDDLE_X = 8, CHANGING_GRAV_MIDDLE_Y = 7, CHANGING_GRAV_LARGE_X = 10, CHANGING_GRAV_LARGE_Y = 9;
    static const int CENTER_OF_GRAV_SMALL_X_AND_Y = 5, CENTER_OF_GRAV_MIDDLE_X_AND_Y = 7, CENTER_OF_GRAV_LARGE_X_AND_Y = 9;

    int tourAvantEvenement = 3; ///Nombre de tour avant réaction de la partie

    bool insererPionsParDessus = true; ///Insère le pion par dessus ?

    bool tourJoueur1 = true; ///Tour au joueur 1 ?
    bool leJoueurAJoue = false; ///Le joueur a joué ?

    bool tempsLimiteActif = false; ///Le temps est limité pour chaque coup ?
    float tempsLimite; ///La limite de temps

    bool iAActive = false; ///L'IA est active ?

    int modeDeJeu; /// Mode de jeu

    Grille_c *grille = nullptr; ///La grille de jeu

    IntArt *intArt = nullptr; /// L'IA

    void declencherEvenement(); ///Déclenche un évènement

    bool tomberAuMilieu(); ///Fait tomber les pions au milieu ?

public:
    Partie_c();

    Partie_c(int modeJeu, unsigned int largeurGrille, unsigned int hauteurGrille, bool estTempsLimite, unsigned int tempsLimite, bool iAActive, unsigned int niveauIA);

    Partie_c(const Partie_c &partie);

    virtual ~Partie_c();

    /**
     * @brief Obtient une référence vers la grille
     * 
     * @return const Grille_c& 
     */
    const Grille_c & getGrille() const;

    /**
     * @brief Fait progrésser le scenario d'évènement
     * 
     */
    void prochainTourAvantEvenement();

    /**
     * @brief Réagit en fonction des actions des joueurs
     * 
     * @return int 
     */
    virtual int pendreEnMainJeu();

    virtual void dessinerGrille() = 0;
    virtual void nettoyerEcran() = 0;
    virtual void ecrireDetails() = 0;
    virtual int testerVictoire() = 0;
    virtual void faireJouerLesJoueurs() = 0;
};

#endif