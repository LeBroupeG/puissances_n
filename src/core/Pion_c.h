#ifndef PION_C_H
#define PION_C_H

/**
 * @brief Architecture des pions du jeu
 */
class Pion_c{


    public:
    int posX, posY;
    int idJoueur;
    /** 
    *   @brief Constructeur Permettant d'initialiser un pion
    *   @param posX : Position sur X
    *   @param posY : Position sur Y
    **/
   Pion_c();
   Pion_c(const Pion_c& pion);
    Pion_c(int idJouer_);

    /**
     * @brief Fixe la position du pion aux coordonnées posX_, posY_
     * 
     * @param posX_ 
     * @param posY_ 
     */
    void setPos(int posX_, int posY_);
};

#endif