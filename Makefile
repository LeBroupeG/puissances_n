CORE = core/IANoeud.cpp core/GestionPrincipale_c.cpp core/Grille_c.cpp core/IntArt_c.cpp core/Partie_c.cpp core/Partie_ia_c.cpp core/Pion_c.cpp

SRCS_TXT = $(CORE) txt/GestionPrincipale_t.cpp txt/Grille_t.cpp txt/main_t.cpp txt/Partie_t.cpp txt/Pion_t.cpp
FINAL_TARGET_TXT = txt
#DEFINE_TXT = -DJEU_TXT

SRCS_SFML = $(CORE) sfml/Bouton.cpp sfml/GestionPrincipale_g.cpp sfml/Grille_g.cpp sfml/main_g.cpp sfml/Partie_g.cpp sfml/Pion_g.cpp sfml/RoundedRectangleShape.cpp sfml/TempsAnim.cpp
FINAL_TARGET_SFML = sfml
#DEFINE_SDL = -DJEU_SDL

ifeq ($(OS),Windows_NT)
	INCLUDE_DIR_SFML = 	-Iextern/SFML-2.5.1/include

	LIBS_SFML = -Lextern \
			-Lextern/SFML-2.5.1/lib \
			-lmingw32 -lsfml-graphics -lsfml-audio -lsfml-window -lsfml-system \

else
	INCLUDE_DIR_SFML = -Iextern/SFML-2.5.1/include
	LIBS_SFML =	-Lextern/SFML-2.5.1/lib\
				-lsfml-graphics -lsfml-audio -lsfml-window -lsfml-system
endif

CC					= g++
LD 					= g++
LDFLAGS  			= -Wl,--rpath=./extern/SFML-2.5.1/lib
CPPFLAGS 			= -Wall -ggdb   #-O2   # pour optimiser
OBJ_DIR 			= obj
SRC_DIR 			= src
BIN_DIR 			= bin
INCLUDE_DIR			= -Isrc -Isrc/core -Isrc/sfml -Isrc/txt



default: make_dir $(BIN_DIR)/$(FINAL_TARGET_TXT) $(BIN_DIR)/$(FINAL_TARGET_SFML)

make_dir:
ifeq ($(OS),Windows_NT)
	if not exist $(OBJ_DIR) mkdir $(OBJ_DIR) $(OBJ_DIR)\txt $(OBJ_DIR)\sfml $(OBJ_DIR)\core
else
	test -d $(OBJ_DIR) || mkdir -p $(OBJ_DIR) $(OBJ_DIR)/txt $(OBJ_DIR)/sfml $(OBJ_DIR)/core
	
endif

$(BIN_DIR)/$(FINAL_TARGET_TXT): $(SRCS_TXT:%.cpp=$(OBJ_DIR)/%.o)
	$(LD) $(CPPFLAGS) $+ -o $@ $(LDFLAGS)

$(BIN_DIR)/$(FINAL_TARGET_SFML): $(SRCS_SFML:%.cpp=$(OBJ_DIR)/%.o)
	$(LD)  $(CPPFLAGS) $(LDFLAGS) $+ -o $@ $(LDFLAGS) $(LIBS_SFML)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC)  $(CPPFLAGS) -c $(CPPFLAGS) $(INCLUDE_DIR_SFML) $(INCLUDE_DIR) $< -o $@

clean:
ifeq ($(OS),Windows_NT)
	del /f $(OBJ_DIR)\txt\*.o $(OBJ_DIR)\sfml\*.o $(OBJ_DIR)\core\*.o $(BIN_DIR)\$(FINAL_TARGET_TXT).exe $(BIN_DIR)\$(FINAL_TARGET_SFML).exe
else
	rm -rf $(OBJ_DIR) $(BIN_DIR)/$(FINAL_TARGET_TXT) $(BIN_DIR)/$(FINAL_TARGET_SFML)
endif

