# Puissances_n

Ce projet est un puissance 4 agrémenté de quelques fonctionnalités supplémentaires réalisé dans le cadre de l'UE LIFAP4 de l'université Lyon 1

######################################### -> Windows <- ##############################################

Pour le compiler sur windows il faut installer make et minGW32

Pour make : installer https://chocolatey.org/install chocolatey.
 Pour ce faire il faut exécuter dans un terminal Powershell en administrateur : 
 "Get-ExecutionPolicy"
 -> si et seulement si cette commande retourne Resticted alors taper : " Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')) "

 Une fois l'installation de chocolatey complétée il suffit de taper  "choco install make"
 
 Pour Mingw 32 :
Si il n'est pas installer, il vous faudra installer MinGW grâce au lien suivant :
https://sourceforge.net/projects/mingw/ avec les paramètres d'installation par défaut
Une fois le programme ouvert, il vous faudra installer au minimum les extensions :
mingw-developer-toolkit
mingw32-base
mingw32-gcc-g++
msys-base

Une fois make et MinGW32 installer, il vous suffira d'exécuter la commande " make " dans le dossier où se trouve le fichier Makefile.

Pour exécuter le programme, lancer le programme en tapant ./bin/sfml.exe pour la version graphique ou ./bin/txt.exe pour la version terminal depuis un terminal à la racine du dossier


Les programmes devraient se lancer.

######################################### -> Linux <- ##############################################
Exécuter la commande make à la racine du dossier


######################################### -> Menu <- ##############################################
Si vous ne voulez pas une option dans le menu, il suffit de ne pas la sélectionner et de passer à la page suivante
lancer le programme en tapant ./bin/sfml pour la version graphique ou ./bin/txt pour la version terminal depuis un terminal à la racine du dossier

######################################### -> Règles du jeu <- ##############################################

Mode :

1) Partie Normale
    Consiste en un puissance 4 classique : aligner 4 pions pour gagner.

2) Shifting vertical
    Tous les 4 tours, chaque pion descend d'une ligne. Les pions de la ligne la plus basse sont ré-insérés sur le dessus.

3) Shifting horizontal
    Tous les 4 tours, chaque pion est décalé d'une colonne à droite. Les pions de la colonne la plus à droite sont ré-insérés à gauche.

4) Insertion alternee
    Tous les 4 tours, l'insertion des pions est changés : (haut->bas), (bas->haut).

5) Gravité changeante
    Tous les 4 tours, la grille effectue un quart de tours dans le sens trigonométrique.

#######################################################################################################
#######################################################################################################
#######################################################################################################



That project is a “Connect 4” with some additional features made for a LIFAP4 course.

######################################### -> Windows <- ##############################################

In order to compile it on windowsd it’s needed to install make and mingw32.
Make : install https://chocolatey.org/install chocolatey
To do that, execute in a Powershell terminal as root :
 "Get-ExecutionPolicy"
 -> if ti returns Resticted , execute : " Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')) "

Once chocolatey installed type “choco install make”
Mingw 32:
If not installed yet, get it from https://sourceforge.net/projects/mingw/ . It’s mandatory to install mingw-developer-toolkit
mingw32-base
mingw32-gcc-g++
msys-base.

If previous steps succeed, use “make” within the game folder root and run it with ./bin/sfml for the nice version or ./bin/txt for the terminal one.

######################################### -> Linux <- ##############################################

If the compilation part failed with linux, please run this command :
export LD_LIBRARY_PATH=./extern/SFML-2.5.1/lib

######################################### -> Menu <- ##############################################
If you do not desire to play with a certain rule, you just have to unselect it and go to the next page

######################################### -> Game rules <- ##############################################

Mode :

1) Partie Normale
    A usual Connect 4 game.

2) Shifting vertical
    Each 4 turns, the pons are moved downward. The lowest ones are re-insert at the top of the grid.

3) Shifting horizontal
    Each 4 turns, the pons are moved rightward. The rightest ones are moved to the leftest column.

4) Insertion alternee
    Each 4 turns, the pon insertion is changed : from the bottom to top and from the top to bottom

5) Gravité changeante
    Each 4 tunrs, the grid is rotated by quater turn counter-clockwize.